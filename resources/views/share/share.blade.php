<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no" />
        <title>分享</title>
        <link rel="stylesheet" type="text/css" href="/share/css/reset.css" />
        <link rel="stylesheet" type="text/css" href="/share/css/share.css" />
    </head>
    <body>
        <main>
            <div class="wrapper">
                <img class="bg" src="/share/img/invite_image_01.png">
                <div class="share">
                    <div class="code">
                        <img src="/share/img/invite_image_02.png" style="height: 17px; line-height: 17px; width: 145px; display: block; text-align: center; margin: 0 auto;">
                        <img id="qrcode" class="reCode" src=""/>
                    </div>
                    <div class="you">
                        <p>您的邀请码: <input type="text"  readonly id="invitation_code"/>
                        <button class="copy" id="copy1">复制邀请码</button></p>
                    </div>
                    <div class="add">
                        <p>下载地址</p>
                        <p><input type="text" readonly id="download_url"/></p>
                        <button class="copy" id="copy2">复制地址</button>
                    </div>
                </div>
                <div class="record">
                    <p class="imgtit"><img src="/share/img/invite_image_04.png"></p>
                    <div class="Tab">
                        <ul class="tab_title" id="tab_title">
                            <li class="active" ><a href="javascript:void(0)" class="current">直推</a><span id="count1">5</span>人</li>
                        </ul>
                        <div class="content" id="container01">
                            <div class="tab con selected" id="tab1">
                                <p><img src="/share/img/invite_image_05.png" style="width: 145px; height: 17px;"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </main>

    </body>
    <script type="text/javascript" src="/share/js/share.js"></script>
    <script type="text/javascript" src="/share/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="/share/js/idevice.js"></script>
    <script>
        //复制按钮事件绑定
        $("#copy1").click(function(){
            //获取input对象
            var obj = document.getElementById("invitation_code");
            //如果是ios端
            if(iDevice != undefined){
                // 获取元素内容是否可编辑和是否只读
                var editable = obj.contentEditable;
                var readOnly = obj.readOnly;
                // 将对象变成可编辑的
                obj.contentEditable = true;
                obj.readOnly = false;
                // 创建一个Range对象，Range 对象表示文档的连续范围区域，如用户在浏览器窗口中用鼠标拖动选中的区域
                var range = document.createRange();
                //获取obj的内容作为选中的范围
                range.selectNodeContents(obj);
                var selection = window.getSelection();
                selection.removeAllRanges();
                selection.addRange(range);
                obj.setSelectionRange(0, 999999);  //选择范围，确保全选
                //恢复原来的状态
                obj.contentEditable = editable;
                obj.readOnly = readOnly;
                //如果是安卓端
            }else{
                obj.select();
            }
            try{
                if(document.execCommand("Copy","false",null)){
                    alert("复制成功！");
                }else{
                    alert("复制失败！请手动复制！");
                }
            }catch(err){
                alert("复制错误！请手动复制！")
            }
        });
        $("#copy2").click("tap",function(){
            //获取input对象
            var obj = document.getElementById("download_url");
            //如果是ios端
            if(iDevice != undefined){
                // 获取元素内容是否可编辑和是否只读
                var editable = obj.contentEditable;
                var readOnly = obj.readOnly;
                // 将对象变成可编辑的
                obj.contentEditable = true;
                obj.readOnly = false;
                // 创建一个Range对象，Range 对象表示文档的连续范围区域，如用户在浏览器窗口中用鼠标拖动选中的区域
                var range = document.createRange();
                //获取obj的内容作为选中的范围
                range.selectNodeContents(obj);
                var selection = window.getSelection();
                selection.removeAllRanges();
                selection.addRange(range);

                obj.setSelectionRange(0, 999999);  //选择范围，确保全选
                //恢复原来的状态
                obj.contentEditable = editable;
                obj.readOnly = readOnly;
                //如果是安卓端
            }else{
                obj.select();
            }
            try{
                if(document.execCommand("Copy","false",null)){
                    alert("复制成功！");
                }else{
                    alert("复制失败！请手动复制！");
                }
            }catch(err){
                alert("复制错误！请手动复制！")
            }
        });

        //数据动态获取 插入
        $(function() {
            var id = '{{ $id }}';
            var url = '{{ $url }}';
            var url = url+'/api/v1/users/'+id+'/share';
            
            $.ajax({
                url: url ,
                success: function (result) {
                    if (result.code == 0) {
                        var html = "";
                        $("#qrcode").attr("src", result.data.qrcode);
                        $("#count1").text(result.data.count1);
                        $("#count2").text(result.data.count2);
                        $("#invitation_code").val(result.data.invitation_code);
                        $("#download_url").val(result.data.download_url);
                        for (var i = 0; i <= result.data.children.length-1; i++) {
                            if (i==5){break;}
                            html +='<div class="item children"><img src="'+result.data.children[i].avatar_small+'" /><span id="name">'+result.data.children[i].name+'</span></div>';
                        }
                        $("#tab1").append(html);
                    } else {
                        alert(result.msg);
                    }
                }
            })
        });
    </script>
</html>