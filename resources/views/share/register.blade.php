<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no" />
		<title>VRTS 注册</title>
		<link rel="stylesheet" type="text/css" href="/register/css/reset.css"/>
		<link rel="stylesheet" type="text/css" href="/register/css/register.css"/>
	</head>
	<body>
	<header>
		<div class="logo">
			<img src="/register/img/编组.png"/>
		</div>
	</header>
	<main>
		<input type="text" id="userName" placeholder="用户名"/>
		<div class="pass">
		<input type="password" id="userPass" placeholder="密码"/>
		</div>
		<input type="text" id="nickName" placeholder="昵称"/>
		<div id="v_container"></div>
		<input type="text" id="invitation_code" readonly placeholder="邀请码"/>
        <div id="v_container"></div>
        <div id="tip"></div>
        <a style="color:#2F54EB" href="{{ $url }}/api/v1/users/down ">下载App注册</a>
        <input type="submit" value="注 册" id="button"/>
	</main>
    <script src="/ub/js/jquery-3.3.1.min.js"></script>
	<script>
        $(function() {
            var _url = '{{ $url }}';
            var _invitation_code = '{{ $invitation_code }}';
            $('#invitation_code').val(_invitation_code);
            $("#button").click(function() {
                
                var name = $("#userName").val();
                var password = $("#userPass").val();
                var nickname = $("#nickName").val();
                var invitation_code = $("#Invitation_Code").val();
                if(name.length < 4 || name.length > 15 || name == "") {
                    $("#tip").html("用户名必须为4-15位字符，请重新输入！")
                    setTimeout(function(){$("#tip").html("")}, 2000);
                } else {
                    if(password.length < 6 || password == "") {
                        $("#tip").html("密码必须大于6位字符，请重新输入！")
                        setTimeout(function(){$("#tip").html("")}, 2000);
                    } else if(nickname == "") {
                        $("#tip").html("请输入昵称！")
                        setTimeout(function(){$("#tip").html("")}, 2000);
                    }else {
                        var _ajax_url = _url+'/api/v1/register';
                        var _falg = true;
                        if(_falg){
                            $.ajax({
                                url: _ajax_url,
                                data: {
                                    name: name,
                                    password: password,
                                    nickname: nickname,
                                    invitation_code: _invitation_code
                                },
                                async: true,
                                type: "POST",
                                success: function(result) {
                                    _falg = false;
                                    if (result.code == 0) {
                                        window.location.href= _url+'/api/v1/users/down';
                                    } else {
                                        _falg = true;
                                        alert(result.msg);
                                    }
                                }
                            });
                        }

                    }
                }
            })
        });
	</script>
	</body>
</html>
