<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<title>VRTS官方下载</title>
		<link rel="stylesheet" type="text/css" href="/ub/css/index.css" />

		<style>
			img {
				max-width: 100%;
			}
		</style>
	</head>

	<body>
		<div class="main margintop">
			<div class="main-left">
				<ul>
					<li>
						<div class="left1">
							<span class="tu">
									<img src="/ub/img/yuan.png"/>
									<div class="bg_cover">
										<p>1</p>
									</div>
								</span>
							<span class="z">VRTS</span>
						</div>
					</li>
					<li><span class="tu1"><img src="/ub/img/xiala.png"/></span>
						<span>安装手机APP,请使用浏览器扫码下载</span>
					</li>
					<div class="bb">
						<li class="b ios">
							<div class="c" style="width:100%;">
								<img class="img" src="/ub/img/iphone.png" />
								<a href="" id="file_ios" class="title">iphone下载</a>
							</div>
							<img class="d" id="ios" src="/share/img/ios.png" style=" width: 70%; height: 70%; margin: 5% 0; border-radius: 5%;" />
						</li>
						<li class="b android">
							<div class="c" style="width: 100%;">
								<img class="img" src="/ub/img/android.png" />
								<a href="" id="file" class="title">Android下载</a>
							</div>
							<img class="d" id="Android" src="" style="width: 70%; height: 70%;margin: 5% 0 ;border-radius: 5%;" />
						</li>
					</div>
				</ul>
			</div>

		</div>
		<div class="clear"></div>
      <div>
        </div>
	</body>


	<script type="text/javascript" src="/ub/js/jquery-3.3.1.min.js"></script>
	<script type='text/javascript'>
		var _file = '{{ $file }}';
		var _qrcode = '{{ $qrcode }}';
		var _file_ios = '{{ $file_ios }}';
        $("#Android").attr("src", _qrcode);
				$("#file").attr("href", _file);
				$("#file_ios").attr("href", _file_ios);
		var Ios = document.getElementsByClassName("ios");
		var Android = document.getElementsByClassName("android");

		var u = navigator.userAgent;
		var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android终端
		var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端

		if(isiOS) {
				Android[0].style.display = 'none'
		}else {
				Ios[0].style.display = 'none'
		}
	</script>
</html>