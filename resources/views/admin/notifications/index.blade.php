<div class="col-md-10 col-lg-offset-1">
    <div class="box">
        <div class="box-body">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <th style="width: 10px">排序</th>
                    <th>用户名</th>
                    <th>时间</th>
                    <th>审核</th>
                </tr>
                        @if ($notifications->count())
                            @foreach ($notifications as $k=>$notification)
                            <tr>
                                <td>
                                    {{$k+1}}
                                </td>
                                    @include('admin.notifications._' . snake_case(class_basename($notification->type)))
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4">
                                    暂无消息通知!
                                </td>
                            </tr>
                        @endif
                </tbody></table>
        </div>
        <div class="box-footer clearfix">
            <ul class="pagination pagination-sm no-margin pull-right">
                {!! $notifications->render() !!}
            </ul>
        </div>
    </div>
</div>