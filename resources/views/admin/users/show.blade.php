<?php
use App\Models\User;

?>
<div class="box box-info container row col-6">
    <div class="box-header with-border">
        <h3 class="box-title">用户状态：{{ User::$stautsMap[$user->status] }}</h3>
        <div class="box-tools">
            <div class="btn-group float-right" style="margin-right: 10px">
                <a href="{{ route('users.index') }}" class="btn btn-sm btn-default"><i class="fa fa-list"></i> 列表</a>
            </div>
        </div>
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tbody>
            <tr>
                <td>用户名：</td>
                <td  colspan="3">{{ $user->name }}</td>
                <td>vrt 总计：</td>
                <td  colspan="3">{{ $user->total_vrt }}</td>
            </tr>
            <tr>
                <td>vrt 值：</td>
                <td>{{ $user->vrt }}</td>
                <td>VRTS 值：</td>
                <td>{{ $user->vrts }}</td>
                <td>YEC 值：</td>
                <td>{{ $user->yec }}</td>
                <td>ETH 值：</td>
                <td>{{ $user->eth }}</td>
            </tr>
                <h3 class="box-title">交易列表：</h3>
            <tr>
                <td rowspan="{{ $user->ethRecord->count() + 1 }}">交易列表</td>
                <td>交易ID</td>
                <td>交易值</td>
                <td>转入类型</td>
                <td>交易状态</td>
                <td>交易时间</td>
                <td>支付地址</td>
            </tr>
            @foreach($user->ethRecord as $k => $item)
                @if($k<10)
                    <tr>
                        <td>{{ $item->id }}  </td>
                        <td>{{ $item->value }}  </td>
                        <td>{{ \App\Models\EthRecord::$typeMap[$item->type] }}  </td>
                        <td>{{ $item->status?'已审核':'未审核'}}  </td>
                        <td>{{ $item->created_at }}  </td>
                        <td>{{ $item->from_address }}  </td>

                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
        
    </div>
    <div class="box-header with-border">
        <h3 class="box-title">用户关系图：</h3>
        <div id="main" style="width: 100%;height:600px;"></div>

    </div>
</div>
<script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/echarts-all-3.js"></script>

<script type="text/javascript">
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('main'));
    myChart.showLoading();
    $.get("{{ url('/admin/relation/'.$user->id) }}", function (data) {
            myChart.hideLoading();
            myChart.setOption(option = {
                tooltip: {
                    trigger: 'item',
                    triggerOn: 'mousemove'
                },
                series:[
                    {
                        type: 'tree',
                        data: [data],
                        left: '2%',
                        right: '2%',
                        top: '8%',
                        bottom: '20%',
                        symbol: 'emptyCircle',
                        orient: 'vertical',
                        expandAndCollapse: true,
                        label: {
                            normal: {
                                position: 'top',
                                rotate: -90,
                                verticalAlign: 'middle',
                                align: 'right',
                                fontSize: 19
                            }
                        },

                        leaves: {
                            label: {
                                normal: {
                                    position: 'bottom',
                                    rotate: -90,
                                    verticalAlign: 'middle',
                                    align: 'left'
                                }
                            }
                        },
                        animationDurationUpdate: 750
                    }
                ]
            });
        });

</script>
