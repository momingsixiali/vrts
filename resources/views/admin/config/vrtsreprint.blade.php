<form class="form-horizontal">
    @csrf
    <div class="box-body ">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">动态规则释放方向至</label>
            <div class="col-sm-5">
                <input type="radio" name="vrt_vrts"  value="1" @if($vrtsReprint->vrtsreprint['vrt_vrts'] == 1) checked @endif > VRTS
                <input type="radio" name="vrt_vrts"  value="0" @if($vrtsReprint->vrtsreprint['vrt_vrts'] == 0) checked @endif > VRT
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="col-sm-offset-2 col-sm-5">
        <button type="reset" class="btn btn-default">重置</button>
        @if($vrtsReprint)
        <a class="btn btn-info pull-right" href="javascript:;" onclick="submit()">更新</a>
        @else
        <a class="btn btn-info pull-right" href="javascript:;" onclick="submit()">提交</a>
        @endif
    </div>
</form>
<script>
    function submit(){
        var vrt_vrts = $("input[name='vrt_vrts']:checked").val();
        console.log(vrt_vrts);
        $.ajaxSetup({
            headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' }
        });
        $.ajax({
            url: "/admin/vrtsreprint",
            async: false,
            data: {vrt_vrts:vrt_vrts},
            type: "POST",
            dataType: "json",
            success: function (obj) {
                if (obj[0] == 'success') {
                    toastr.success(obj[1]);
                }
            }
        });
    }
</script>