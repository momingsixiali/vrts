<form class="form-horizontal">
    @csrf
    <div class="box-body ">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">VRTS - ¥</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" min="0" max="100" id="vrts" value="{{$multipleratios?$multipleratios->multipleratios['vrts']:''}}" placeholder="">
                <span id="error1"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">VRT - ¥</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" min="0" max="100" id="vrt" value="{{$multipleratios?$multipleratios->multipleratios['vrt']:''}}" placeholder="">
                <span id="error2"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">YEC - ¥</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" min="0" max="100" id="yec" value="{{$multipleratios?$multipleratios->multipleratios['yec']:''}}" placeholder="">
                <span id="error3"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">ETH - ¥</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" min="0" max="100" id="eth" value="{{$multipleratios?$multipleratios->multipleratios['eth']:''}}" placeholder="">
                <span id="error4"></span>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="col-sm-offset-2 col-sm-5">
        <button type="reset" class="btn btn-default">重置</button>
        @if($multipleratios)
        <a class="btn btn-info pull-right" href="javascript:;" onclick="submit()">更新</a>
        @else
        <a class="btn btn-info pull-right" href="javascript:;" onclick="submit()">提交</a>
        @endif

    </div>
</form>
<script>
    function submit(){
        var _vrts = $("#vrts").val();
        var _vrt = $("#vrt").val();
        var _yec = $("#yec").val();
        var _eth = $("#eth").val();
        if(_vrts.length == 0 ){
                $("#error1").html("该数据不能为空!");
                $("#error1").css({"color":"red"});
                setTimeout(function(){$("#error1").html("")}, 2000);
                return false;
        }
        if(_vrt.length == 0 ){
                $("#error2").html("该数据不能为空!");
                $("#error2").css({"color":"red"});
                setTimeout(function(){$("#error2").html("")}, 2000);
                return false;
        }
        if(_yec.length == 0 ){
                $("#error3").html("该数据不能为空!");
                $("#error3").css({"color":"red"});
                setTimeout(function(){$("#error3").html("")}, 2000);
                return false;
        }
        if(_eth.length == 0 ){
                $("#error4").html("该数据不能为空!");
                $("#error4").css({"color":"red"});
                setTimeout(function(){$("#error4").html("")}, 2000);
                return false;
        }
        
        $.ajaxSetup({
            headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' }
        });
        $.ajax({
            url: "/admin/multipleratios",
            async: false,
            data: {yec:_yec,vrt:_vrt,vrts:_vrts,eth:_eth},
            type: "POST",
            dataType: "json",
            success: function (obj) {
                if (obj[0] == 'success') {
                    toastr.success(obj[1]);
                }
            }
        });
    }
</script>