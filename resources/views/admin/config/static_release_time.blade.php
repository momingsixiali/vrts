<form class="form-horizontal" id="throw">
    @csrf
    <div class="box-body ">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">红包有效时间</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" min="0" max="10" id="effective_time" value="{{$static_release_time?$static_release_time->static_release_time['effective_time']:''}}" placeholder="单位小时" required>
                <span id="error"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">静态红包释放时间</label>
            <div class="col-sm-5">
                <input type="time" class="form-control" id="release_date" value="{{$static_release_time?$static_release_time->static_release_time['release_date']:''}}" placeholder="18:00" required pattern="[0-9]{2}:[0-9]{2}">
                <span id="error1"></span>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="col-sm-offset-2 col-sm-5">
        <button type="reset" class="btn btn-default">重置</button>
        @if($static_release_time)
        <a class="btn btn-info pull-right" href="javascript:;" onclick="submit()">更新</a>
        @else
        <a class="btn btn-info pull-right" href="javascript:;" onclick="submit()">提交</a>
        @endif

    </div>
</form>
<script>
    function submit(){
        var effective_time = $("#effective_time").val();
        var release_date = $("#release_date").val();
        if(effective_time.length == 0 ){
            $("#error").html("红包有效时间不能为空!");
            $("#error").css({"color":"red"});
            setTimeout(function(){$("#error").html("")}, 2000);
            return false;
        }
        if (effective_time>24) {
            $("#error").html("红包有效时间不能大于24!");
            $("#error").css({"color":"red"});
            setTimeout(function(){$("#error").html("")}, 2000);
            return false;
        }
        if(release_date.length == 0 ){
                $("#error1").html("静态释放时间不能为空!");
                $("#error1").css({"color":"red"});
            setTimeout(function(){$("#error1").html("")}, 2000);
            return false;
        }

        $.ajaxSetup({
            headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' }
        });
        $.ajax({
            url: "/admin/staticreleasetime",
            async: false,
            data: {effective_time:effective_time,release_date:release_date},
            type: "POST",
            dataType: "json",
            success: function (obj) {
                if (obj[0] == 'success') {
                    toastr.success(obj[1]);
                }
            }
        });
    }
</script>