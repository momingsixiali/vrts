<form class="form-horizontal" id="yec_eth">
    @csrf
    <div class="box-body ">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">YEC与RMB比率</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" min="0" max="10" id="yec_rmb" value="{{$yec_eth?$yec_eth->yec_eth['yec_rmb']:''}}" placeholder="" required>
                <span id="error"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">ETH与RMB比率</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" min="0" max="100" id="eth_rmb" value="{{$yec_eth?$yec_eth->yec_eth['eth_rmb']:''}}" placeholder="" required>
                <span id="error1"></span>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="col-sm-offset-2 col-sm-5">
        <button type="reset" class="btn btn-default">重置</button>
        @if($yec_eth)
        <a class="btn btn-info pull-right" href="javascript:;" onclick="submit()">更新</a>
        @else
        <a class="btn btn-info pull-right" href="javascript:;" onclick="submit()">提交</a>
        @endif

    </div>
</form>
<script>
    function submit(){
        var yec_rmb = $("#yec_rmb").val();
        var eth_rmb = $("#eth_rmb").val();
        if(yec_rmb.length == 0 ){
            $("#error").html("该数据不能为空!");
            $("#error").css({"color":"red"});
            setTimeout(function(){$("#error").html("")}, 2000);
            return false;
        }
        if (yec_rmb>1000) {
            $("#error").html("不能大于1000!");
            $("#error").css({"color":"red"});
            setTimeout(function(){$("#error").html("")}, 2000);
            return false;
        }
        if(eth_rmb.length == 0 ){
                $("#error1").html("该数据不能为空!");
                $("#error1").css({"color":"red"});
            setTimeout(function(){$("#error1").html("")}, 2000);
            return false;
        }
        if (eth_rmb>10000) {
            $("#error1").html("不能大于10000!");
            $("#error1").css({"color":"red"});
            setTimeout(function(){$("#error1").html("")}, 2000);
            return false;
        }
        $.ajaxSetup({
            headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' }
        });
        $.ajax({
            url: "/admin/yeceth",
            async: false,
            data: {yec_rmb:yec_rmb,eth_rmb:eth_rmb},
            type: "POST",
            dataType: "json",
            success: function (obj) {
                if (obj[0] == 'success') {
                    toastr.success(obj[1]);
                }
            }
        });
    }
</script>