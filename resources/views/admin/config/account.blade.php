<form class="form-horizontal">
    @csrf
    <div class="box-body ">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">转入方获得VRTS</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" min="0" max="100" id="to_vrts" value="{{$account_id_one?$account_id_one->accounts['to_vrts']:''}}" placeholder="转入方获得VRTS:0.8">
                <span id="error"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">转入方获得VRT</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" min="0" max="100" id="to_vrt" value="{{$account_id_one?$account_id_one->accounts['to_vrt']:''}}" placeholder="转入方获得VRT:0.2">
                <span id="error1"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">转出方获得VRT</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" min="0" max="100" id="from_vrt" value="{{$account_id_one?$account_id_one->accounts['from_vrt']:''}}" placeholder="转出方获得VRT:0.8">
                <span id="error2"></span>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="col-sm-offset-2 col-sm-5">
        <button type="reset" class="btn btn-default">重置</button>
        @if($account_id_one)
        <a class="btn btn-info pull-right" href="javascript:;" onclick="submit()">更新</a>
        @else
        <a class="btn btn-info pull-right" href="javascript:;" onclick="submit()">提交</a>
        @endif

    </div>
</form>
<script>
    function submit(){
        var to_vrts = $("#to_vrts").val();
        var to_vrt = $("#to_vrt").val();
        var from_vrt = $("#from_vrt").val();
        if(to_vrts.length == 0 ){
                $("#error").html("该数据不能为空!");
                $("#error").css({"color":"red"});
                setTimeout(function(){$("#error").html("")}, 2000);
                return false;
        }
        if(to_vrt.length == 0 ){
                $("#error1").html("该数据不能为空!");
                $("#error1").css({"color":"red"});
            setTimeout(function(){$("#error1").html("")}, 2000);

            return false;
        }
        if(from_vrt.length == 0 ){
                $("#error2").html("该数据不能为空!");
                $("#error2").css({"color":"red"});
            setTimeout(function(){$("#error2").html("")}, 2000);
            return false;
        }
        $.ajaxSetup({
            headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' }
        });
        $.ajax({
            url: "/admin/account",
            async: false,
            data: {to_vrts:to_vrts,to_vrt:to_vrt,from_vrt:from_vrt},
            type: "POST",
            dataType: "json",
            success: function (obj) {
                if (obj[0] == 'success') {
                    toastr.success(obj[1]);
                }
            }
        });
    }
</script>