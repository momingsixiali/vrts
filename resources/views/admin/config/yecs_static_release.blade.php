<form class="form-horizontal" id="throw">
    @csrf
    <div class="box-body ">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">YECS 静态释放倍数</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" min="0" max="100" id="yec_yecs_ratio" value="{{$yecs_static_release?$yecs_static_release->yecs_static_release['yec_yecs_ratio']:''}}" placeholder="释放倍数" required>
                <span id="error"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">YECS 静态释放时间</label>
            <div class="col-sm-5">
                <input type="time" class="form-control" id="release_time" value="{{$yecs_static_release?$yecs_static_release->yecs_static_release['release_time']:''}}" placeholder="18:00" required pattern="[0-9]{2}:[0-9]{2}">
                <span id="error1"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">YECS 静态释放是否开启</label>
            <div class="col-sm-5">
                <input type="radio" name="enable" value="1" @if($yecs_static_release->yecs_static_release['enable'] == 1) checked @endif> 开启
                <input type="radio" name="enable" value="0" @if($yecs_static_release->yecs_static_release['enable'] == 0) checked @endif > 关闭
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="col-sm-offset-2 col-sm-5">
        <button type="reset" class="btn btn-default">重置</button>
        @if($yecs_static_release)
        <a class="btn btn-info pull-right" href="javascript:;" onclick="submit()">更新</a>
        @else
        <a class="btn btn-info pull-right" href="javascript:;" onclick="submit()">提交</a>
        @endif

    </div>
</form>
<script>
    function submit(){
        var yec_yecs_ratio = $("#yec_yecs_ratio").val();
        var release_time = $("#release_time").val();
        var enable = $("input[name='enable']:checked").val();
        if(yec_yecs_ratio.length == 0 ){
            $("#error").html("YECS 静态释放倍数不能为空!");
            $("#error").css({"color":"red"});
            setTimeout(function(){$("#error").html("")}, 2000);
            return false;
        }
        if (yec_yecs_ratio>100) {
            $("#error").html("YECS 静态释放倍数不能大于100!");
            $("#error").css({"color":"red"});
            setTimeout(function(){$("#error").html("")}, 2000);
            return false;
        }
        if(release_time.length == 0 ){
            $("#error1").html("YECS 静态释放时间不能为空!");
            $("#error1").css({"color":"red"});
            setTimeout(function(){$("#error1").html("")}, 2000);
            return false;
        }

        $.ajaxSetup({
            headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' }
        });
        $.ajax({
            url: "/admin/yecsstaticrelease",
            async: false,
            data: {yec_yecs_ratio:yec_yecs_ratio,release_time:release_time,enable:enable},
            type: "POST",
            dataType: "json",
            success: function (obj) {
                if (obj[0] == 'success') {
                    toastr.success(obj[1]);
                }
            }
        });
    }
</script>