<form class="form-horizontal">
    @csrf
    <div class="box-body ">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">是否开启</label>
            <div class="col-sm-5">
                <input type="radio" name="open_activation" id="open_activation" value="1" @if($activation->activation['open_activation'] == 1) checked @endif > 开启
                <input type="radio" name="open_activation" id="open_activation" value="0" @if($activation->activation['open_activation'] == 0) checked @endif> 关闭
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">扣除YEC数量</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" min="0" max="100" id="deduction_yec" value="{{$activation?$activation->activation['deduction_yec']:''}}" placeholder="扣除YEC数量">
                <span id="error"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">扣除ETH数量</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" min="0" max="100" id="deduction_eth" value="{{$activation?$activation->activation['deduction_eth']:''}}" placeholder="扣除ETH数量">
                <span id="error1"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">激活赠送条件</label>
            <div class="col-sm-5">
                <input type="radio" name="give" id="give" value="1" @if($activation->activation['give'] == 1) checked @endif> 开启
                <input type="radio" name="give" id="give" value="0" @if($activation->activation['give'] == 0) checked @endif> 关闭
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">推荐人比例</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" min="0" max="100" id="recommendation_rate" value="{{$activation?$activation->activation['recommendation_rate']:''}}" placeholder="推荐人比例">
                <span id="error2"></span>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="col-sm-offset-2 col-sm-5">
        <button type="reset" class="btn btn-default">重置</button>
        @if($activation)
        <a class="btn btn-info pull-right" href="javascript:;" onclick="submit()">更新</a>
        @else
        <a class="btn btn-info pull-right" href="javascript:;" onclick="submit()">提交</a>
        @endif

    </div>
</form>
<script>
    function submit(){
        var open_activation = $("input[name='open_activation']:checked").val();
        var give = $("input[name='give']:checked").val();

        var deduction_eth = $("#deduction_eth").val();
        var deduction_yec = $("#deduction_yec").val();
        var recommendation_rate = $("#recommendation_rate").val();
        if(deduction_yec.length == 0 ){
            $("#error").html("该数据不能为空!");
            $("#error").css({"color":"red"});
            setTimeout(function(){$("#error").html("")}, 2000);
            return false;
        }
        if(deduction_eth.length == 0 ){
            $("#error1").html("该数据不能为空!");
            $("#error1").css({"color":"red"});
            setTimeout(function(){$("#error").html("")}, 2000);
            return false;
        }
        if(recommendation_rate.length == 0 ){
                $("#error2").html("该数据不能为空!");
                $("#error2").css({"color":"red"});
                setTimeout(function(){$("#error").html("")}, 2000);
                return false;
        }

        $.ajaxSetup({
            headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' }
        });
        $.ajax({
            url: "/admin/activation",
            async: false,
            data: {open_activation:open_activation,deduction_yec:deduction_yec,deduction_eth:deduction_eth,recommendation_rate:recommendation_rate,give:give},
            type: "POST",
            dataType: "json",
            success: function (obj) {
                if (obj[0] == 'success') {
                    toastr.success(obj[1]);
                }
            }
        });
    }
</script>