<form class="form-horizontal" id="throw">
    @csrf
    <div class="box-body ">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">首次复投</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" min="0" max="10" id="throw_first" value="{{$throw_id_one?$throw_id_one->throw['throw_first']:''}}" placeholder="5" required>
                <span id="error"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">复投</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" min="0" max="100" id="throw_second" value="{{$throw_id_one?$throw_id_one->throw['throw_second']:''}}" placeholder="" required>
                <span id="error1"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">一共可复投次数</label>
            <div class="col-sm-5">
                <input type="number" class="form-control" min="0" max="100" id="throw_sum" value="{{$throw_id_one?$throw_id_one->throw['throw_sum']:''}}" placeholder="" required>
                <span id="error2"></span>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="col-sm-offset-2 col-sm-5">
        <button type="reset" class="btn btn-default">重置</button>
        @if($throw_id_one)
        <a class="btn btn-info pull-right" href="javascript:;" onclick="submit()">更新</a>
        @else
        <a class="btn btn-info pull-right" href="javascript:;" onclick="submit()">提交</a>
        @endif

    </div>
</form>
<script>
    function submit(){
        var throw_first = $("#throw_first").val();
        var throw_second = $("#throw_second").val();
        var throw_sum = $("#throw_sum").val();
        if(throw_first.length == 0 ){
            $("#error").html("该数据不能为空!");
            $("#error").css({"color":"red"});
            setTimeout(function(){$("#error").html("")}, 2000);
            return false;
        }
        if (throw_first>10) {
            $("#error").html("首次复投率不能不能大于10!");
            $("#error").css({"color":"red"});
            setTimeout(function(){$("#error").html("")}, 2000);
            return false;
        }
        if(throw_second.length == 0 ){
                $("#error1").html("该数据不能为空!");
                $("#error1").css({"color":"red"});
            setTimeout(function(){$("#error1").html("")}, 2000);
            return false;
        }
        if (throw_second>10) {
            $("#error1").html("复投率不能大于10!");
            $("#error1").css({"color":"red"});
            setTimeout(function(){$("#error1").html("")}, 2000);
            return false;
        }
        if(throw_sum.length == 0 ){
                $("#error2").html("该数据不能为空!");
                $("#error2").css({"color":"red"});
            setTimeout(function(){$("#error2").html("")}, 2000);
            return false;
        }
        if (throw_sum>100) {
            $("#error2").html("复投次数不能大于100!");
            $("#error2").css({"color":"red"});
            setTimeout(function(){$("#error2").html("")}, 2000);
            return false;
        }
        $.ajaxSetup({
            headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' }
        });
        $.ajax({
            url: "/admin/throw",
            async: false,
            data: {throw_first:throw_first,throw_second:throw_second,throw_sum:throw_sum},
            type: "POST",
            dataType: "json",
            success: function (obj) {
                if (obj[0] == 'success') {
                    toastr.success(obj[1]);
                }
            }
        });
    }
</script>