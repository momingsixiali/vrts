@if(Admin::user()->notification_count && Admin::user()->can('ethrecordobserver' ))
<li  style="font-size: 19px;">
    <a style="width: -1px;height: 15px;border-radius: 15px;margin-top: 10px;line-height: 0px;" class="label-{{ Admin::user()->notification_count > 0 ? 'danger' : 'primary' }}"  href="{{ route('notifications.index') }}">
        {{ Admin::user()->notification_count?Admin::user()->notification_count:'' }}
    </a>
</li>
@endif
