<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::post('/login', 'LoginController@index');
    Route::get('versions', 'VersionController@index');
    Route::post('/register', 'RegisterController@index');
    Route::get('/users/{id}/share', 'UserController@share');
    Route::get('/users/getshare', 'UserController@getShare');
    Route::get('/pc/register', 'UserController@register');
    Route::get('/users/down', 'UserController@download');
    Route::post('/reset/password', 'ResetPasswordController@index');
    Route::get('/forgot/password', 'ForgotPasswordController@index');
});

Route::prefix('v1')->middleware('auth:api')->group(function () {
    Route::get('/bips', 'UserController@bips');
    Route::get('/users', 'UserController@show');
    Route::post('/users/verify', 'UserController@verify');
    //实名认证
    Route::post('/authentication', 'UserController@authentication');
    Route::post('/verificationCodes', 'UserController@sms');

    Route::put('/users', 'UserController@update');
    Route::post('/images', 'ImageController@store');

    Route::get('/addresses', 'AddressController@index');
    Route::post('/addresses', 'AddressController@store');
    Route::delete('/addresses/{id}', 'AddressController@destroy');

    Route::middleware('pay.passwd')->group(function () {
        Route::post('/withdraws', 'WithdrawController@store');
        Route::post('/transfers', 'TransferController@store');
    });

    Route::get('/withdraws', 'WithdrawController@index');

    // 兑换页面
    Route::get('exchanges', 'ExchangeController@show');
    Route::post('exchanges', 'ExchangeController@store');
    Route::get('exchanges/list', 'ExchangeController@index');

    Route::get('/assets', 'UserController@assets');
    Route::get('/wallets', 'WalletController@show');
    Route::get('/receipts', 'ReceiptController@index');
    Route::get('/transfers', 'TransferController@index');

    Route::post('/recharges', 'RechargeController@store');
    Route::get('/recharges/list', 'RechargeController@index');
    // 充值时获取平台的收款地址和二维码链接
    Route::get('/recharges/rules', 'RechargeController@rules');

    // 我的资产
    Route::get('/yec', 'UserController@yec');
    Route::get('/eth', 'UserController@eth');
    Route::get('/vrt', 'UserController@vrt');
    Route::get('/vrts', 'UserController@vrts');
    Route::get('/yecs', 'UserController@yecs');

    Route::get('/packets', 'PacketController@index');
    Route::get('/packets/status', 'PacketController@status');
    Route::get('/packets/available', 'PacketController@count');

    Route::get('/packets/{id}', 'PacketController@update');

    Route::get('/repeats', 'RepeatController@index');
    Route::post('/repeats', 'RepeatController@store');
});

Route::get('/test', 'Api\TestController@test')->name('test');
