<?php

use Jpush\Client;
use Illuminate\Support\Str;
use Moontoast\Math\BigNumber;
use GuzzleHttp\Client as GuzzleClient;

if (!function_exists('jpush')) {

    /**
     * 获取 jpush client
     *
     * @return Client
     */
    function jpush()
    {
        return app('jpush');
    }
}

if (!function_exists('storage_url')) {

    /**
     * @param  string  $path
     * @return string
     */
    function storage_url(string $path)
    {
        if (Str::startsWith($path, ['http://', 'https://'])) {
            return $path;
        }

        return url('storage/' . $path);
    }
}

if (!function_exists('big_number')) {

    /**
     * 自定义方法 默认的精度为小数点后八位
     * @param $number
     * @param  int  $scale
     * @return BigNumber
     */
    function big_number($number, $scale = 8)
    {
        return new BigNumber($number, $scale);
    }
}

if (!function_exists('get_eth_client')) {

    /**
     * @return GuzzleClient
     */
    function get_eth_client()
    {
        return new GuzzleClient(['base_uri' => config('vrts.eth_url')]);
    }
}
