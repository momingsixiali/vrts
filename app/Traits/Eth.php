<?php

namespace App\Traits;

use App\Models\Address;

trait Eth
{
    public function withdraw($addressId, $value)
    {
        $client = get_eth_client();

        $address = Address::find($addressId);

        $res = $client->post('/sendTsByAccount', [
            'form_params' => [
                'number' => (string) abs($value),
                'toaddress' => $address->address,
            ]
        ]);

        $data = json_decode($res->getBody(), true)['data'];

        return $data['address'];
    }
}
