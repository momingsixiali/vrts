<?php

namespace App\Traits;

use App\Models\Packet;
use Illuminate\Support\Facades\Log;

/**
 * 红包的消息推送
 *
 * Trait RedPacketPush
 * @package App\Traits
 */
trait RedPacketPush
{
    public function send(Packet $packet)
    {
        try {
            $message = $packet->toArray();
            jpush()->push()->setPlatform(['android'])
                ->addAlias("user_$packet->user_id")
                ->setNotificationAlert($message)
                ->send();
        } catch (\Throwable $exception) {
            Log::error('sending error: ' . $exception->getMessage());
        }
    }
}
