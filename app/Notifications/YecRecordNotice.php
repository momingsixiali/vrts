<?php

namespace App\Notifications;

use App\Models\YecRecord;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class YecRecordNotice extends Notification
{
    use Queueable;

    public $yec_record;

    public function __construct(YecRecord $yec_record)
    {
        $this->yec_record = $yec_record;
    }

    public function via($notifiable)
    {
        return ['database'];
    }

    public function toDatabase($notifiable)
    {
        $yec_record = $this->yec_record;
        $link = $yec_record->link([$this->yec_record->id]);
        // 存入数据库里的数据
        return [
            'yec_record_id' => $this->yec_record->id,
            'yec_record_name' => $this->yec_record->user->name,
            'value' => $this->yec_record->value,
            'user_id' => $this->yec_record->user_id,
            'to_address_id' => $this->yec_record->to_address_id,
            'from_address_id' => $this->yec_record->from_address_id,
            'type' => $this->yec_record->type,
            'enable_value'=>$this->yec_record->enable_value,
            'enable_yecs_value'=>$this->yec_record->enable_yecs_value,
            'link' => $link,
            'title' => '有新的审核',
        ];
    }
}
