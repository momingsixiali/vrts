<?php

namespace App\Console;

use App\Tasks\YecQuota;
use App\Tasks\Recharge;
use App\Tasks\UserLevel;
use App\Tasks\VrtStaticRelease;
use App\Tasks\YecsStaticRelease;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(new UserLevel())->dailyAt('00:10');
        $schedule->call(new VrtStaticRelease())->dailyAt('01:10');
        $schedule->call(new YecsStaticRelease())->dailyAt('01:10');
        //$schedule->call(new Recharge())->everyMinute();
        $schedule->call(new Recharge())->dailyAt('02:00');
        $schedule->call(new YecQuota())->dailyAt('02:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
