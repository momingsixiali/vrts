<?php

namespace App\Http\Controllers\V1;

use App\Models\Banner;

class WalletController extends ApiController
{
    public function show()
    {
        $user = $this->getUser();

        $now = now();
        $x = [$now->toDateString()];
        for ($i = 1; $i < 7; $i++) {
            $x[] = $now->subDay()->toDateString();
        }

        $banners = Banner::whereEnabled(true)->get()->toArray();
        $banners = array_map(function ($banner) {
            return [
                'title' => 'title',
                'url' => $banner['path']
            ];
        }, $banners);
        $data = [
            'charts' => [
                'title' => 'YEC 兑 ETH',
                'x' => array_reverse($x),
                // 'pointStrokeColor' => '#fff',
                // 'pointHighlightFill' => '#fff',
                // 'pointColor' => 'rgba(220,220,220,1)',
                // 'fillColor' => 'rgba(220,220,220,0.2)',
                // 'strokeColor' => 'rgba(220,220,220,1)',
                // 'pointHighlightStroke' => 'rgba(220,220,220,1)',
                'data' => ['100', '200', '300', '400', '300', '200', '100'],
            ],
            'assets' => [
                'vrt' => $user->vrt,
                'yec' => $user->yec,
                'vrts' => $user->vrts,
                'yecs' => $user->yecs,
                'eth' => (string) number_format($user->eth - $user->eth_freeze, 8)
            ],
            'banners' => $banners
        ];

        return $this->success($data);
    }
}
