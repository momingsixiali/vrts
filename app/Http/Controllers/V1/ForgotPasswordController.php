<?php

namespace App\Http\Controllers\V1;

use App\Models\User;
use App\Support\Code;
use App\Http\Requests\Request;

class ForgotPasswordController extends ApiController
{
    public function index(Request $request)
    {
        $user = User::where('name', $request->name)->first();
        if (!$user) {
            return $this->fail(Code::PARAMETER_ERROR, '用户名不存在！');
        }

        $mnemonics = $user->mnemonic_word;
        $data['order'] = $mnemonics;
        shuffle($mnemonics);
        $data['disorder'] = $mnemonics;
        return $this->success($data);
    }
}
