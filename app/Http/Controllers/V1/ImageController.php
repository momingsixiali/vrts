<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class ImageController extends ApiController
{
    public function store(Request $request)
    {
        $avatarBig = Storage::disk('public')
            ->putFile('avatars', $request->file('avatar'));

        $avatarSmall = 'avatars/small' . explode('/', $avatarBig)[1];

        Image::make($request->file('avatar'))->resize(74, 74)
            ->save(storage_path() . '/app/public/' . $avatarSmall);

        return $this->success([
            'avatar_big' => storage_url($avatarBig),
            'avatar_small' => storage_url($avatarSmall)
        ]);
    }
}
