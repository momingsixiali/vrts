<?php

namespace App\Http\Controllers\V1;

use App\Models\User;
use App\Support\Code;

abstract class ApiController
{
    protected $msg = 'success';
    protected $code = Code::SUCCESS;

    protected function setMsg($msg)
    {
        $this->msg = $msg;
        return $this;
    }

    protected function success($data = '')
    {
        $rtn = ['code' => $this->code, 'msg' => $this->msg, 'data' => $data];
        if (app()->environment() !== 'production') {
            $rtn['request'] = request()->all();
        }
        return response()->json($rtn);
    }

    protected function fail(int $code, string $msg)
    {
        $rtn = ['code' => $code, 'msg' => $msg, 'data' => ''];
        if (app()->environment() !== 'production') {
            $rtn['request'] = request()->all();
        }
        return response()->json($rtn);
    }

    /**
     * @return User
     */
    protected function getUser()
    {
        return auth()->user();
    }
}
