<?php

namespace App\Http\Controllers\V1;

use App\Support\Code;
use Illuminate\Http\Request;

class LoginController extends ApiController
{
    public function index(Request $request)
    {
        $credentials = [
            'name' => $request->name,
            'password' => $request->password
        ];
        if (auth()->attempt($credentials)) {
            $user = $this->getUser();
            $token = $user->createToken('api')->accessToken;

            return $this->success([
                'name' => $user->name,
                'status' => $user->status,
                'token' => 'Bearer ' . $token,
                'mnemonic_word' => $user->mnemonic_word
            ]);
        } else {
            return $this->fail(Code::PARAMETER_ERROR, '密码不正确');
        }
    }
}
