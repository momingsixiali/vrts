<?php

namespace App\Http\Controllers\V1;

use App\Models\VrtsRecord;
use App\Http\Resources\VrtsRecordCollection;

class ReceiptController extends ApiController
{
    /**
     * 收款记录
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $user = $this->getUser();
        return $this->success(new VrtsRecordCollection(
            VrtsRecord::where('user_id', $user->id)
                ->where('type', VrtsRecord::TYPE_RECEIPT)
                ->orderByDesc('created_at')->paginate()));
    }

}
