<?php

namespace App\Http\Resources;

use App\Models\Packet;
use App\Support\Currency;
use Illuminate\Support\Str;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PacketCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map(function (Packet $packet) {
            if ($packet->expired_at < now() && $packet->status == Packet::STATUS_UNCLAIMED) {
                $packet->update(['status' => Packet::STATUS_EXPIRED]);
            }
            return [
                'id' => $packet->id,
                'type' => $packet->type,
                'status' => $packet->status,
                'created_at' => (string) $packet->created_at,
                'value' => (string) number_format($packet->value, 5),
                'currency' => Str::upper(Currency::getStrByCode($packet->currency))
            ];
        });
    }
}
