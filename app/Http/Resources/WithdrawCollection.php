<?php

namespace App\Http\Resources;

use App\Models\Address;
use App\Support\Currency;
use Illuminate\Support\Str;
use Illuminate\Http\Resources\Json\ResourceCollection;

class WithdrawCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map(function ($item) {
            // todo 这里要删除
            $item->currency = Currency::ETH;
            return [
                'value' => $item->value,
                'created_at' => (string) $item->created_at,
                'currency' => Str::upper(Currency::getStrByCode($item->currency)),
                'address' => Address::withTrashed()->find($item->to_address_id)->address
            ];
        });
    }
}
