<?php

namespace App\Http\Resources;

use App\Models\Address;
use Illuminate\Http\Resources\Json\ResourceCollection;

class RechargeCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map(function ($item) {
            $data = [
                'status' => $item->status,
                'value_1' => $item->enable_value,
                'currency_1' => $item->currency_1,
                'created_at' => (string) $item->created_at,
                'updated_at' => (string) $item->updated_at,
                'value_2' => $item->enable_yecs_value ?? '',
                'address' => Address::withTrashed()->find($item->address_id)->address ?? ''
            ];

            if ($data['currency_1'] == 'YEC') {
                $data['currency_2'] = 'YECS';
            } else {
                $data['currency_2'] = '';
            }

            return $data;
        });
    }
}
