<?php

namespace App\Http\Resources;

use App\Support\Currency;
use Illuminate\Support\Str;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ExchangeCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map(function ($item) {
            return [
                'to_value' => $item->to_value,
                'from_value' => $item->from_value,
                'created_at' => (string) $item->created_at,
                'to_currency' => Str::upper(Currency::getStrByCode($item->to_currency)),
                'from_currency' => Str::upper(Currency::getStrByCode($item->from_currency))
            ];
        });
    }
}
