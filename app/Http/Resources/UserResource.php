<?php

namespace App\Http\Resources;

use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            // TODO
            'id' => $this->id,
            'name' => $this->name,
            'status' => $this->status,
            'id_card' => $this->id_card,
            'phone' => $this->phone ?? '',
            'nickname' => $this->nickname,
            'real_name' => $this->real_name,
            'avatar_big' => $this->avatar_big,
            'avatar_small' => $this->avatar_small,
            'mnemonic_word' => $this->mnemonic_word,
            'wallet_address' => $this->wallet_address,
            'recharge_currency' => [
                ['value' => 1, 'currency' => 'YEC'],
                ['value' => 4, 'currency' => 'ETH'],
            ],
            'pay_passwd' => is_null($this->pay_passwd) ? false : true,
            'wallet_address_qrcode' => $this->getWalletAddressQrcode()
        ];
    }

    protected function getWalletAddressQrcode()
    {
        $file = 'qrcodes/' . md5($this->wallet_address) . '.png';

        if (!Storage::disk('public')->exists($file)) {
            Storage::disk('public')->put($file,
                QrCode::format('png')->size(224)->generate($this->wallet_address));
        }

        return storage_url($file);
    }

}
