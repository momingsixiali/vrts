<?php

namespace App\Http\Requests;

class VerificationCodeRequest extends Request
{
    public function rules()
    {
        return [
            'phone' => [
                'required',
                'regex:/^[1](([3][0-9])|([4][5-9])|([5][0-3,5-9])|([6][5,6])|([7][0-8])|([8][0-9])|([9][1,8,9]))[0-9]{8}$/',
                'unique:users'
            ]
        ];
    }
}
