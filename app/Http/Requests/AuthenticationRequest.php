<?php

namespace App\Http\Requests;

class AuthenticationRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'card' => [
                'required',
                'regex:/(^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$)|(^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}$)/',
            ],
            'real_name'=>[
                'required'
            ],
            'verification_code'=>[
                'required'
            ],
            'verification_key'=>[
                'required'
            ]
        ];
    }

    public function attributes()
    {
        return [
            'verification_key' => '短信验证码 key',
            'verification_code' => '短信验证码',
            'real_name' => '姓名',
            'card' => '身份证号',
        ];
    }
}
