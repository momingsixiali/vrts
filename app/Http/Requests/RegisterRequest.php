<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nickname' => 'required|string|max:25',
            'password' => 'required|string|max:25|min:6',
            'invitation_code' => 'required|string|nullable|min:6|max:6',
            'name' => 'required|unique:users,name|letter|max:25'
        ];
    }
  
  	public function attributes()
    {
        return [
            'nickname' =>'昵称',
            'invitation_code' => '邀请码',
            'password' => '密码',
            'name' => '用户名',

        ];
    }
}
