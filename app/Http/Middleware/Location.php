<?php

namespace App\Http\Middleware;

class Location
{
    public function handle($request, \Closure $next)
    {
        $response = $next($request);
        $accountId = !empty(config('userinfo')['account']['id']) ? config('userinfo')['account']['id'] : null;
        $userId = !empty(config('userinfo')['user']['id']) ? config('userinfo')['user']['id'] : null;

        $content = json_decode($response->getContent(), true); // 有时间看下 getContent 和 getData 的联系
        $error = [];
        if (isset($content['code']) && $content['code'] != 0) {
            $error['code'] = $content['code'];
            $error['message'] = $content['message'];
            if (!empty($content['debug']['file'])) {
                $error['file'] = $content['debug']['file'];
            }
            if (!empty($content['debug']['line'])) {
                $error['line'] = $content['debug']['line'];
            }
        }

        return $response;
    }
}

