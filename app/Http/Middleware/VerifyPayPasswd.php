<?php

namespace App\Http\Middleware;

use App\Support\Code;
use Illuminate\Http\Request;

class VerifyPayPasswd
{
    public function handle(Request $request, \Closure $next)
    {
        if (!$request->has('pay_passwd')) {
            return response()->json([
                'data' => '',
                'msg' => '支付密码是必须的',
                'code' => Code::PARAMETER_ERROR
            ]);
        }

        if (auth()->user()->pay_passwd != $request->pay_passwd) {
            return response()->json([
                'data' => '',
                'msg' => '支付密码错误',
                'code' => Code::PARAMETER_ERROR
            ]);
        }

        return $next($request);
    }
}
