<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSwitch extends Model
{
    const VRTS_ACCOUNTS_STATUS_OFF = 0; // VRTS 转账 关闭
    const VRTS_ACCOUNTS_STATUS_ON = 1; // VRTS 转账 开启

    const STATIC_STATUS_OFF = 0; // 静态释放
    const STATIC_STATUS_ON = 1;

    const DYNAMIC_STATUS_OFF = 0; // 动态释放
    const DYNAMIC_STATUS_ON = 1;

    public static $vrtsStautsMap = [
        self::VRTS_ACCOUNTS_STATUS_OFF   => '关闭',
        self::VRTS_ACCOUNTS_STATUS_ON   => '开启',
    ];
    public static $staticStautsMap = [
        self::STATIC_STATUS_OFF   => '关闭',
        self::STATIC_STATUS_ON   => '开启',
    ];
    public static $dynamicStautsMap = [
        self::DYNAMIC_STATUS_OFF   => '关闭',
        self::DYNAMIC_STATUS_ON   => '开启',
    ];
    
    public $fillable = [
        'user_id',
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
