<?php

namespace App\Models;

use Illuminate\Support\Str;

class Address extends BaseModel
{
    const TYPE_PLATFORM = 0;
    const TYPE_RECHARGE = 1;
    const TYPE_WITHDRAW = 2;

    const CURRENCY_YEC  = 1;
    const CURRENCY_VRT  = 2;
    const CURRENCY_VRTS = 3;
    const CURRENCY_ETH  = 4;

    const PLATFORM_USER_ID = 0;

    public static $typeMap = [
        self::TYPE_PLATFORM   => '平台地址',
        self::TYPE_RECHARGE   => '充值地址',
        self::TYPE_WITHDRAW   => '提币地址',
    ];

    public static $currencyMap = [
        self::CURRENCY_YEC   => 'YEC',
        self::CURRENCY_VRT   => 'VRT',
        self::CURRENCY_VRTS  => 'VRTS',
        self::CURRENCY_ETH   => 'ETH',
    ];

    protected $fillable = [
        'type',
        'remark',
        'address',
        'user_id',
        'currency',
        'status',
        'qrcode',
    ];

    protected $casts = [
        'status'=>'boolean',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getQrcodeAttribute($value)
    {
        // 如果 image 字段本身就已经是完整的 url 就直接返回
        if (Str::startsWith($value, ['http://', 'https://'])) {
            return $value;
        }

        if ($value) {
            $value = storage_url($value);
        }

        return $value;
    }
}
