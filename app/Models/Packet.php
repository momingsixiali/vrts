<?php

namespace App\Models;

use App\Support\Currency;

class Packet extends BaseModel
{
    const TYPE_DAY = 1;
    const TYPE_TEAM = 2;

    const STATUS_UNCLAIMED = 0;
    const STATUS_CLAIMED = 1;
    const STATUS_EXPIRED = 2;

    // todo 定义常亮
    public static $statusMap = [
        0 => '未领取',
        1 => '已领取',
        2 => '已过期',
    ];

    protected $fillable = ['user_id', 'value', 'currency', 'type', 'status', 'expired_at'];

    public function getTypeAttribute($value)
    {
        if ($value === self::TYPE_DAY) {
            if ($this->currency === Currency::YEC) {
                $type = 'YEC 红包';
            } else {
                $type = '签到红包';
            }
        } else {
            $type = '团队红包';
        }
        return $type;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function record()
    {
        return $this->morphOne(Record::class, 'recordable');
    }
}
