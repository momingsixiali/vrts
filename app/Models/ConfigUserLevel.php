<?php

namespace App\Models;

use App\Admin\Controllers\ConfigDynamicReleaseController;

class ConfigUserLevel extends BaseModel
{
    protected $fillable = [
        'level',
        'scale',
        'vrt_set',
        'vrt_direct_total',
        'vrt_indirect_total',
        'vrt_direct_repeat_total',
        'vrt_indirect_repeat_total',
        'computational_algebra',
    ];

    public function configDynamicRelease()
    {
        return $this->hasOne(ConfigDynamicRelease::class,'scale_id','scale');
    }

    public function configStaticRelease()
    {
        return $this->hasOne(ConfigStaticRelease::class,'scale_id','scale');
    }

    public function configYecsStaticRatio()
    {
        return $this->hasOne(ConfigYecsStaticRatio::class,'scale_id','scale');
    }
    public function getVrtSetAttribute($value)
    {
        if (request()->path()=='admin/userlevelset') {
            if ($this->scale > 1) {
                $str = 'N > '.str_replace('.00000000', '',$value);
            }else{
                $str = '0 <N<= '.str_replace('.00000000', '',$value);
            }
        }else{
            $str = str_replace('.00000000', '',$value);
        }

        return $str;
    }

    public function getVrtDirectTotalAttribute($value)
    {
        if (request()->path()=='admin/userlevelset') {
            if ($value == 0) {
                $str = str_replace('.00000000', '', $value);
            } else {
                $str = 'N > ' . str_replace('.00000000', '', $value);
            }
        }else{
                $str = str_replace('.00000000', '',$value);
            }
        return $str;
    }

    public function getVrtIndirectTotalAttribute($value)
    {
        if (request()->path()=='admin/userlevelset') {
            if ($value == 0) {
                $str = str_replace('.00000000', '',$value);
            }else{
                $str = 'N > '.str_replace('.00000000', '',$value);
            }
        }else{
            $str = str_replace('.00000000', '',$value);
        }
        return $str;
    }

    public function getVrtDirectRepeatTotalAttribute($value)
    {
        if (request()->path()=='admin/userlevelset') {
            if ($value == 0) {
                $str = str_replace('.00000000', '', $value);
            } else {
                $str = 'N > ' . str_replace('.00000000', '', $value);
            }
        }else{
            $str = str_replace('.00000000', '', $value);
        }
        return $str;
    }

    public function getVrtIndirectRepeatTotalAttribute($value)
    {
        if (request()->path()=='admin/userlevelset') {
            if ($value == 0) {
                $str = str_replace('.00000000', '', $value);
            } else {
                $str = 'N > ' . str_replace('.00000000', '', $value);
            }
        }else{
            $str = str_replace('.00000000', '', $value);
        }
        return $str;
    }

}
