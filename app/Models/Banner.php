<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Banner extends Model
{
    const TYPE_banner = 1;
    const TYPE_other = 2;
    public static $typeMap = [
        self::TYPE_banner   => 'Banner图',
        self::TYPE_other   => '其他图',
    ];
    protected $fillable = [
        'path','type'
    ];

    public function getPathAttribute()
    {
        // 如果 image 字段本身就已经是完整的 url 就直接返回
        if (Str::startsWith($this->attributes['path'], ['http://', 'https://'])) {
            return $this->attributes['path'];
        }
        if (app()->environment('local')) {
            return \Storage::disk('public')->url($this->attributes['path']);
        } elseif (app()->environment('test')) {
            return 'http://192.168.100.113/storage/'.$this->attributes['path'];
        } else {
            return storage_url($this->attributes['path']);
        }
    }
}
