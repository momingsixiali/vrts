<?php

namespace App\Models;

class ConfigStaticRelease extends BaseModel
{
    protected $fillable = [
        'level',
        'scale_id',
        'vrt_one',
        'vrt_two',
        'vrt_three',
        'vrt_four',
        'vrt_five',
    ];

    public function userLevel()
    {
        return $this->belongsTo(ConfigUserLevel::class,'scale_id','scale');
    }
}
