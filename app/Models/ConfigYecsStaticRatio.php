<?php

namespace App\Models;

class ConfigYecsStaticRatio extends BaseModel
{
    protected $fillable = [
        'level',
        'scale_id',
        'yecs_one',
        'yecs_two',
        'yecs_three',
        'yecs_four',
        'yecs_five',
    ];

    public function userLevel()
    {
        return $this->belongsTo(ConfigUserLevel::class,'scale_id','scale');
    }
}
