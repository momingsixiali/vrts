<?php

namespace App\Models;

class Exchange extends BaseModel
{
    protected $fillable = [
        'user_id',
        'to_value',
        'from_value',
        'to_currency',
        'from_currency'
    ];

    public function records()
    {
        return $this->morphMany(Record::class, 'recordable');
    }
}
