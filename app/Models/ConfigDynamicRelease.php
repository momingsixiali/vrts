<?php

namespace App\Models;

class ConfigDynamicRelease extends BaseModel
{
    protected $fillable = [
        'level',
        'scale_id',
        'dynamic_direct_rule',
        'dynamic_indirect_rule',
        'dynamic_direct_repeat_rule',
        'dynamic_indirect_repeat_rule',
        'dynamic_one',
        'dynamic_two',
        'dynamic_three',
        'dynamic_four',
        'dynamic_five',
    ];

    public function userLevel()
    {
        return $this->belongsTo(ConfigUserLevel::class,'scale_id','scale');
    }
}
