<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConfigMultipleRule extends Model
{
    public static $states = [
        'on'  => ['value' => 1, 'text' => '开启', 'color' => 'success'],
        'off' => ['value' => 0, 'text' => '关闭', 'color' => 'danger'],
    ];
    protected $fillable = [
        'accounts',
        'throw',
        'static_release_time',
        'yecs_static_release',
        'yec_eth',
        'activation',
        'multipleratios',
        'vrtsreprint',
    ];

    protected $casts = [
        'accounts' => 'array',
        'throw' => 'array',
        'static_release_time' => 'array',
        'yecs_static_release' => 'array',
        'yec_eth' => 'array',
        'activation'=>'array',
        'multipleratios'=>'array',
        'vrtsreprint'=>'array',
    ];
}
