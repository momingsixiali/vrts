<?php

namespace App\Models;

class Record extends BaseModel
{
    protected $fillable = [
        'type',
        'value',
        'user_id',
        'currency'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function recordable()
    {
        return $this->morphTo();
    }
}
