<?php

namespace App\Models;

class ConfigExchanges extends BaseModel
{
    const TYPE_YEC_VRT = 1;
    const TYPE_YEC_ETH = 2;
    const TYPE_ETH_VRTS = 4;
    const TYPE_VRTS_ETH = 5;
    const TYPE_YEC_VRTS = 6;
    const TYPE_VRTS_YEC = 7;
    const TYPE_ETH_YEC = 3;
    const ENABLED = 1;
    public static $typeMap = [
        self::TYPE_YEC_VRT   => 'YEC兑换VRT',
        self::TYPE_YEC_ETH   => 'YEC兑换ETH',
        self::TYPE_VRTS_ETH   => 'VRTS兑换ETH',
        self::TYPE_ETH_VRTS   => 'ETH兑换VRTS',

        self::TYPE_YEC_VRTS => 'YEC兑换VRTS',
        self::TYPE_VRTS_YEC => 'VRTS兑换YEC',
        self::TYPE_ETH_YEC   => 'ETH兑换YEC',
    ];

    public static $typeValue = [
        self::TYPE_YEC_VRT   => '1 YEC = value VRT',
        self::TYPE_YEC_ETH   => '1 YEC =  value ETH',
        self::TYPE_ETH_YEC   => '1 ETH = value YEC',
        self::TYPE_ETH_VRTS   => '1 ETH = value VRTS',
        self::TYPE_VRTS_ETH   => '1 VRTS = value ETH',

        self::TYPE_YEC_VRTS   => '1 YEC = value VRTS',
        self::TYPE_VRTS_YEC   => '1 VRTS = value YEC',
    ];
    protected $fillable = [
        'type',
        'tip',
        'exchange_value',
        'not_before',
        'not_after',
        'enabled',
        'yec_rmb',
        'eth_rmb',
    ];
    protected $casts = [
        'enabled' => 'boolean',
    ];
    protected $dates = ['not_before', 'not_after'];
}
