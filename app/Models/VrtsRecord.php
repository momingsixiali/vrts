<?php

namespace App\Models;

class VrtsRecord extends BaseModel
{
    const TYPE_TRANSFER = 1; // 转账
    const TYPE_RECEIPT = 2;  // 收款

    protected $fillable = ['user_id', 'value', 'vrt_value', 'address', 'type'];

    public function records()
    {
        return $this->morphMany(Record::class, 'recordable');
    }
}
