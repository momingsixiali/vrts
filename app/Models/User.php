<?php

namespace App\Models;

use App\Support\Currency;
use Illuminate\Support\Str;
use Moontoast\Math\BigNumber;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use HasApiTokens;

    const STATUS_UNVERIFIED = 0; // 未验证
    const STATUS_NOT_ACTIVE = 1; // 未激活
    const STATUS_ACTIVATED = 2;  // 已激活
    const STATUS_PROHIBIT = 3;  // 禁用
    const STATUS_LOGINRESTRICTION = 4;  // 登录限制

    public static $stautsMap = [
        self::STATUS_UNVERIFIED   => '未验证',
        self::STATUS_NOT_ACTIVE   => '未激活',
        self::STATUS_ACTIVATED   => '已激活',
        self::STATUS_PROHIBIT   => '禁用',
        self::STATUS_LOGINRESTRICTION   => '登录限制',
    ];
    
    protected $fillable = [
        'password',
        'pay_passwd',
        'invitation_code'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * 这个是给 auth()->attempt($credentials) 使用的。
     * @return string
     */
    public function getAuthPassword()
    {
        // 注册时密码加密时返回
        // return $this->password;

        // 注册时密码没加密时返回
        return bcrypt($this->password);
    }
    /**
     * 生成邀请码
     */
    public static function getInvitationCode()
    {
        do {
            $invitationCode = Str::random(6);
        } while (User::where('invitation_code', $invitationCode)->count());

        return $invitationCode;
    }
    /**
     * 获取助记词数组
     *
     * @param $value
     * @return array
     */
    public function getMnemonicWordAttribute($value)
    {
        return explode(' ', $value);
    }

    // 充值地址
    public function rechargeAddresses()
    {
        return $this->hasMany(Address::class)->where('type', 1)->withTrashed();
    }

    // 提币地址
    public function withdrawAddresses()
    {
        return $this->hasMany(Address::class)->where('type', 2)->withTrashed();
    }

    public function addresses()
    {
        return $this->hasMany(Address::class)->withTrashed();
    }
    
    public function userSwitch()
    {
        return $this->hasOne(UserSwitch::class);
    }

    public function childrens()
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    // 交易记录
    public function ethRecord()
    {
        return $this->hasMany(EthRecord::class);
    }

    public function getEnableValueByCurrency(int $currency)
    {
        if (!$this->exists) {
            return 0;
        }

        $column = Currency::getStrByCode($currency);

        return $this->{$column} - $this->{$column . '_freeze'};
    }

    /**
     * 添加复投值
     * @param $value
     */
    public function addDoubleVrt($value)
    {
        if (!$this->exists) {
            return;
        }

        if ($value instanceof BigNumber) {
            $value = $value->abs();
        } else {
            $value = abs($value);
        }

        $column = 'double_vrt';
        $this->{$column} = big_number($this->{$column})->add($value);
        $this->save();
    }

    public function addFreezeByCurrency(int $currency, $value)
    {
        if (!$this->exists) {
            return;
        }

        $column = Currency::getStrByCode($currency) . '_freeze';
        $this->{$column} = big_number($this->{$column})->add($value);
        $this->save();
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    //获取该 ID 下所有的子集
    public function children()
    {
        return $this->childrens()->with('children');
    }
}
