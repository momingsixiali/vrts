<?php

namespace App\Models;

class ConfigRecharge extends BaseModel
{
    const TYPE_YEC = 1;
    const TYPE_ETH = 2;
    public static $typeMap = [
        self::TYPE_YEC   => 'YEC充值',
        self::TYPE_ETH   => 'ETH充值',
    ];
    protected $fillable = [
        'type',
        'quota',
        'quota_surplus',
    ];
    protected $casts = [
        'enabled' => 'boolean',
    ];
    protected $dates = ['opening_time'];
}
