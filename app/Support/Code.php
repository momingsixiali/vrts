<?php

namespace App\Support;

class Code
{
    const SUCCESS = 0;
    const NOT_EXISTS = 1; // 资源不存在
    const PARAMETER_ERROR = 2; // 参数错误
    const SERVER_ERROR = 3;   // 服务器错误
}
