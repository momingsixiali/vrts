<?php

namespace App\Support;

use Carbon\Carbon;
use App\Models\Address;
use App\Models\ConfigRecharge;
use App\Models\ConfigExchanges;
use App\Models\ConfigMultipleRule;
use App\Exceptions\ConfigNotSetException;

class Config
{
    public static function ConfigMultipleRules()
    {
        return ConfigMultipleRule::where('id', 1)->first();
    }

    /**
     * 获取 YEC 平台转账地址
     */
    public static function getYecAddress()
    {
        if ($config = Address::where([
            'status' => 1,
            'type'=>Address::TYPE_PLATFORM,
            'currency'=>Address::CURRENCY_YEC,
            'user_id'=>Address::PLATFORM_USER_ID
        ])->first()) {
            return $config;
        }
        throw new ConfigNotSetException('YEC 平台转账地址未设置');
    }

    /**
     * 获取 Vrt 静态释放时间/生效时长
     */
    public static function getVrtStaicReleaseTime()
    {
        if ($config = self::ConfigMultipleRules()) {
            if ($config->static_release_time) {
                return $config->static_release_time;
            }
        }
        throw new ConfigNotSetException('Vrt 静态释放时间/生效时长未设置');
    }
    
    /**
     * 我的 多币种比率
     */
    public static function getMultipleRatios()
    {
        if ($config = self::ConfigMultipleRules()) {
            if ($config->multipleratios) {
                return $config->multipleratios;
            }
        }
        throw new ConfigNotSetException('多币种比率未设置');
    }
    
    /**
     * 动态规则释放方向，VRT 0 或 VRTS 1 。
     */
    public static function getVrtsReprint()
    {
        if ($config = self::ConfigMultipleRules()) {
            if ($config->vrtsreprint) {
                return $config->vrtsreprint;
            }
        }
        throw new ConfigNotSetException('动态规则释放方向未设置');
    }

    /**
     * YECS 静态释放倍数
     */
    public static function getYecYecsRatio()
    {
        if ($config = self::ConfigMultipleRules()) {
            return $config->yecs_static_release['yec_yecs_ratio'];
        }
        throw new ConfigNotSetException('YECS 静态释放倍数未设置');
    }

    /**
     * 转账规则
     */
    public static function getAccounts()
    {
        if ($config = self::ConfigMultipleRules()) {
            if ($config->accounts) {
                return $config->accounts;
            }
        }
        throw new ConfigNotSetException('转账规则未设置');
    }

    /**
     * YEC/ETH 与 RMB 兑换比率
     */
    public static function getYecEthRMB()
    {
        if ($config = self::ConfigMultipleRules()) {
            if ($config->yec_eth) {
                return $config->yec_eth;
            }
        }
        throw new ConfigNotSetException('YEC/ETH 与 RMB 兑换比率未设置');
    }

    /**
     * 复投规则
     */
    public static function getThrow()
    {
        if ($config = self::ConfigMultipleRules()) {
            if ($config->throw) {
                return $config->throw;
            }
        }
        throw new ConfigNotSetException('复投规则未设置');
    }

    /**
     * 激活规则
     */
    public static function getActivation()
    {
        if ($config = self::ConfigMultipleRules()) {
            if ($config->activation) {
                return $config->activation;
            }
        }
        throw new ConfigNotSetException('激活规则未设置');
    }

    /**
     * ETH 审核
     */
    public static function getEthEnabled()
    {
        if ($config = self::ConfigMultipleRules()) {
            return true;
            // if ($config->enabled) {
            //     return $config->enabled;
            // }
        }
        throw new ConfigNotSetException('ETH 审核未设置');
    }

    /**
     * 今日YEC限额和余额
     */
    public static function getRecharge()
    {
        if ($config = ConfigRecharge::where(['enabled'=>1,'type'=>ConfigRecharge::TYPE_YEC])
                                    ->where('opening_time', '<=', Carbon::now())
                                    ->orderBy('opening_time', 'desc')
                                    ->first()
        ) {
            if ($config) {
                return $config;
            }
        }
        throw new ConfigNotSetException('今日YEC限额和余额未设置');
    }
    
    /**
     * 今日ETH限额和余额
     */
    public static function getRechargeEth()
    {
        if ($config = ConfigRecharge::where(['enabled'=>1,'type'=>ConfigRecharge::TYPE_ETH])
                                    ->where('opening_time', '<=', Carbon::now())
                                    ->orderBy('opening_time', 'desc')
                                    ->first()
        ) {
            if ($config) {
                return $config;
            }
        }
        throw new ConfigNotSetException('今日ETH限额和余额未设置');
    }

    /**
     * 兑换规则
     */
    public static function getExchanges()
    {
        if ($TYPE_YEC_VRT = ConfigExchanges::where(['enabled'=>ConfigExchanges::ENABLED,'type'=>ConfigExchanges::TYPE_YEC_VRT])->where('not_before', '<=', Carbon::now())
                                            ->orderBy('not_before', 'desc')
                                            ->first()) {
            $TYPE_YEC_VRT->first_rmb = $TYPE_YEC_VRT->eth_rmb;
            $TYPE_YEC_VRT->second_rmb = $TYPE_YEC_VRT->yec_rmb;
            unset($TYPE_YEC_VRT->yec_rmb,$TYPE_YEC_VRT->eth_rmb);
            $config['YEC_VRT'] =  $TYPE_YEC_VRT;
        }
        if ($TYPE_YEC_ETH = ConfigExchanges::where(['enabled'=>ConfigExchanges::ENABLED,'type'=>ConfigExchanges::TYPE_YEC_ETH])
                                            ->where('not_before', '<=', Carbon::now())
                                            ->orderBy('not_before', 'desc')
                                            ->first()) {
            $TYPE_YEC_ETH->first_rmb = $TYPE_YEC_ETH->eth_rmb;
            $TYPE_YEC_ETH->second_rmb = $TYPE_YEC_ETH->yec_rmb;
            unset($TYPE_YEC_ETH->yec_rmb,$TYPE_YEC_ETH->eth_rmb);
            $config['YEC_ETH'] =  $TYPE_YEC_ETH;
        }
        if ($TYPE_ETH_YEC = ConfigExchanges::where(['enabled'=>ConfigExchanges::ENABLED,'type'=>ConfigExchanges::TYPE_ETH_YEC])->where('not_before', '<=', Carbon::now())
                                            ->orderBy('not_before', 'desc')
                                            ->first()) {
            $TYPE_ETH_YEC->first_rmb = $TYPE_ETH_YEC->eth_rmb;
            $TYPE_ETH_YEC->second_rmb = $TYPE_ETH_YEC->yec_rmb;
            unset($TYPE_ETH_YEC->yec_rmb,$TYPE_ETH_YEC->eth_rmb);
            $config['ETH_YEC'] =  $TYPE_ETH_YEC;
        }
        if ($TYPE_ETH_VRTS = ConfigExchanges::where(['enabled'=>ConfigExchanges::ENABLED,'type'=>ConfigExchanges::TYPE_ETH_VRTS])->where('not_before', '<=', Carbon::now())
                                            ->orderBy('not_before', 'desc')
                                            ->first()) {
            $TYPE_ETH_VRTS->first_rmb = $TYPE_ETH_VRTS->eth_rmb;
            $TYPE_ETH_VRTS->second_rmb = $TYPE_ETH_VRTS->yec_rmb;
            unset($TYPE_ETH_VRTS->yec_rmb,$TYPE_ETH_VRTS->eth_rmb);
            $config['ETH_VRTS'] =  $TYPE_ETH_VRTS;
        }
        if ($TYPE_VRTS_ETH = ConfigExchanges::where(['enabled'=>ConfigExchanges::ENABLED,'type'=>ConfigExchanges::TYPE_VRTS_ETH])->where('not_before', '<=', Carbon::now())
                                            ->orderBy('not_before', 'desc')
                                            ->first()) {
            $TYPE_VRTS_ETH->first_rmb = $TYPE_VRTS_ETH->eth_rmb;
            $TYPE_VRTS_ETH->second_rmb = $TYPE_VRTS_ETH->yec_rmb;
            unset($TYPE_VRTS_ETH->yec_rmb,$TYPE_VRTS_ETH->eth_rmb);
            $config['VRTS_ETH'] =  $TYPE_VRTS_ETH;
        }

        if ($TYPE_YEC_VRTS = ConfigExchanges::where(['enabled'=>ConfigExchanges::ENABLED,'type'=>ConfigExchanges::TYPE_YEC_VRTS])->where('not_before', '<=', Carbon::now())
                                            ->orderBy('not_before', 'desc')
                                            ->first()) {
            $TYPE_YEC_VRTS->first_rmb = $TYPE_YEC_VRTS->eth_rmb;
            $TYPE_YEC_VRTS->second_rmb = $TYPE_YEC_VRTS->yec_rmb;
            unset($TYPE_YEC_VRTS->yec_rmb,$TYPE_YEC_VRTS->eth_rmb);
            $config['YEC_VRTS'] =  $TYPE_YEC_VRTS;
        }
        if ($TYPE_VRTS_YEC = ConfigExchanges::where(['enabled'=>ConfigExchanges::ENABLED,'type'=>ConfigExchanges::TYPE_VRTS_YEC])->where('not_before', '<=', Carbon::now())
                                            ->orderBy('not_before', 'desc')
                                            ->first()) {
            $TYPE_VRTS_YEC->first_rmb = $TYPE_VRTS_YEC->eth_rmb;
            $TYPE_VRTS_YEC->second_rmb = $TYPE_VRTS_YEC->yec_rmb;
            unset($TYPE_VRTS_YEC->yec_rmb,$TYPE_VRTS_YEC->eth_rmb);
            $config['VRTS_YEC'] =  $TYPE_VRTS_YEC;
        }
        if (count($config)) {
            return $config;
        }
        throw new ConfigNotSetException('兑换规则未设置');
    }
}
