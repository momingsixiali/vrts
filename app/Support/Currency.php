<?php

namespace App\Support;

class Currency
{
    const YEC = 1;
    const VRT = 2;
    const VRTS = 3;
    const ETH = 4;
    const YECS = 5;

    public static $map = [
        self::YEC => 'yec',
        self::VRT => 'vrt',
        self::VRTS => 'vrts',
        self::ETH => 'eth',
        self::YECS => 'yecs',
    ];

    public static function getStrByCode($code)
    {
        return self::$map[$code];
    }
}
