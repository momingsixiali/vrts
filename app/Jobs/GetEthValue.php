<?php

namespace App\Jobs;

use Throwable;
use App\Models\EthRecord;
use App\Support\Currency;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

/**
 * 获取用户的 eth 充值金额
 *
 * Class GetEthValue
 * @package App\Jobs
 */
class GetEthValue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $ethRecord;

    /**
     * GetEthValue constructor.
     * @param  EthRecord  $ethRecord
     */
    public function __construct(EthRecord $ethRecord)
    {
        $this->ethRecord = $ethRecord;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = get_eth_client();

        $res = $client->post('/getRecordByAddr', [
            'form_params' => [
                'address' => $this->ethRecord->toAddress->address
            ]
        ]);
        $data = json_decode($res->getBody(), true)['data'];
        $value = array_sum($data['price']);
        if ($value > 0) {
            $record = $this->ethRecord;
            $record->status = EthRecord::STATUS_UNCONFIRMED;
            $record->save();
        }
    }
}
