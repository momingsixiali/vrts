<?php

namespace App\Observers;

use App\Models\YecRecord;
use App\Notifications\YecRecordNotice;

class YecRecordObserver
{
    /**
     * 监听创建充值/提现事件.
     *
     * @param  \App\Models\YecRecord  $ethRecord
     * @return void
     */
    public function created(YecRecord $yecRecord)
    {
        //$yecRecord->notify(new YecRecordNotice($yecRecord));
    }
}
