<?php

namespace App\Observers;

use App\Models\Record;
use App\Support\Currency;

class RecordObserver
{
    public function created(Record $record)
    {
        $user = $record->user;
        $column = Currency::getStrByCode($record->currency);
        $user->{$column} = big_number($user->{$column})->add($record->value);
        if ($column === 'yecs' or $column === 'vrt') {
            if ((string) $record->value > 0) {
                $column = 'total_' . $column;
                $user->{$column} = big_number($user->{$column})->add($record->value);
            }
        }
        $user->save();
    }
}
