<?php

namespace App\Observers;

use App\Models\User;
use App\Jobs\DynamicRelease;
use App\Models\UserSwitch;

class UserObserver
{
    public function saved(User $user)
    {
        if (config('api.globals.is_indirect')) {
            return;
        }

        if (!$user->vrt) {
            return;
        }

        $value = (string) $user->vrt - $user->getOriginal('vrt');
        if ($value <= 0) {
            return;
        }

        dispatch(new DynamicRelease($user, $value));
    }

    public function created(User $user)
    {
        UserSwitch::create(
            [
            'user_id' => $user->id,
        ]
        );
    }
}
