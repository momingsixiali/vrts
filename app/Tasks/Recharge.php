<?php

namespace App\Tasks;

use App\Support\Config;

/**
 *YEC限额还原 定时任务
 *
 * Class
 * @package App\Tasks
 */
class Recharge
{
    public function __invoke()
    {
        $getRecharge = Config::getRecharge();
        $getRecharge->quota_surplus =$getRecharge->quota;
        $getRecharge->save();
        \Log::info('YEC限额清零 定时任务结束' .date("Y-m-d H:i:s"));
    }
}
