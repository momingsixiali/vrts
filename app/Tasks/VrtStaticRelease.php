<?php

namespace App\Tasks;

use App\Models\User;
use App\Models\Packet;
use App\Support\Currency;
use Illuminate\Support\Arr;
use App\Traits\RedPacketPush;
use Illuminate\Support\Facades\DB;
use App\Models\ConfigStaticRelease;
use Illuminate\Support\Facades\Log;

/**
 * 静态释放
 *
 * Class UserLevel
 * @package App\Tasks
 */
class VrtStaticRelease
{
    use RedPacketPush;

    public function __invoke()
    {
        // scale_id 待确认
        $configs = ConfigStaticRelease::get()->keyBy('scale_id')->toArray();
        User::get()->each(function (User $user) use ($configs) {

            if ($user->admin_level) {
                $level = $user->admin_level;
            } else {
                $level = $user->level;
            }
            if (!Arr::has($configs, $level)) {
                return;
            }

            $config = $configs[$level];
            $released = Packet::where('user_id', $user->id)
                ->where('currency', Currency::VRTS)->sum('value');

            if ($user->total_vrt == 0) {
                $ratio = 0;
            } else {
                $ratio = $released / $user->total_vrt;
            }

            if ($ratio < 0.2) {
                $value = $user->vrt * $config['vrt_one'];
            } elseif ($ratio < 0.4) {
                $value = $user->vrt * $config['vrt_two'];
            } elseif ($ratio < 0.6) {
                $value = $user->vrt * $config['vrt_three'];
            } elseif ($ratio < 0.8) {
                $value = $user->vrt * $config['vrt_four'];
            } else {
                $value = $user->vrt * $config['vrt_five'];
            }
         	$value = bcdiv($value,100,5);
            //$value = big_number($value,5)->divide('100');
            if ($value > 0) {
                try {
                    DB::transaction(function () use ($value, $user) {
                        $packet = Packet::create([
                            'value' => $value,
                            'user_id' => $user->id,
                            'type' => Packet::TYPE_DAY,
                            'currency' => Currency::VRTS,
                            'expired_at' => now()->addHour(24)->toDateTimeString()
                        ]);
                        $packet->record()->create([
                            'value' => -$value,
                            'type' => '释放至 VRTS',
                            'user_id' => $user->id,
                            'currency' => Currency::VRT
                        ]);

                       // $this->send($packet);
                    });
                } catch (\Throwable $exception) {
                    Log::error('vrt static release error: ' . $exception->getMessage());
                }
            }
        });
    }
}
