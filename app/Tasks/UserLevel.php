<?php

namespace App\Tasks;

use App\Models\User;
use Illuminate\Support\Arr;
use App\Models\ConfigUserLevel;
use Illuminate\Support\Collection;

/**
 * 计算用户等级的定时任务
 *
 * Class UserLevel
 * @package App\Tasks
 */
class UserLevel
{

    /**
     * 关系层级
     *
     * @var int
     */
    protected $i;

    /**
     * @var Collection
     */
    protected $users;


    public function __invoke()
    {	
        $configs = ConfigUserLevel::orderByDesc('scale')
            ->get()->keyBy('scale')->toArray();
        $this->users = User::get();

        $this->users->each(function (User $user) use ($configs) {
            if ($user->admin_level) {
                $level = $user->admin_level;
            } else {
                $level = $user->level;
            }
            if (!Arr::has($configs, $level)) {
                return;
            }

            $config = $configs[$level];
          
            $children = $this->users->where('parent_id', $user->id);
            $user->direct_total_vrt = $children->sum('total_vrt');
          
            $user->direct_double_vrt = $children->sum('double_vrt');
            $this->i = $config['computational_algebra']; // 计算多少代
            $user->indirect_total_vrt = $this->getIndirectTotalVrt($children->pluck('id'));
            $user->indirect_double_vrt = $this->getIndirectDoubleVrt($children->pluck('id'));
         
            foreach ($configs as $scale => $config) {
              // if($user->id == 86){
         // \Log::info($user.'86');
         //        \Log::info($config);
            
        //  }
                if ($user->total_vrt < $config['vrt_set']) continue;
               // if ($user->direct_total_vrt >= $config['vrt_direct_total']) continue;
               // if ($user->indirect_total_vrt >= $config['vrt_indirect_total']) continue;
               // if ($user->direct_double_vrt >= $config['vrt_direct_repeat_total']) continue;
               // if ($user->indirect_double_vrt >= $config['vrt_indirect_repeat_total']) continue;
                $user->level = $scale;
              //	\Log::info($user->id.'等级调整');
                break;
            }
            $user->save();
        });
    }

    /**
     * 获取间推总和
     *
     * @param $ids
     * @param  int  $level
     * @return mixed
     */
    public function getIndirectTotalVrt($ids, $level = 1)
    {
        if ($level >= $this->i) {
            return 0;
        }

        $children = $this->users->whereIn('parent_id', $ids);

        $ids = $children->pluck('id');
        if (!$ids) {
            return 0;
        }

        return $children->sum('total_vrt') + $this->getIndirectTotalVrt($ids, $level + 1);
    }

    /**
     * 获取间推复投总和
     *
     * @param $ids
     * @param  int  $level
     * @return mixed
     */
    public function getIndirectDoubleVrt($ids, $level = 1)
    {
        if ($level >= $this->i) {
            return 0;
        }

        $children = $this->users->whereIn('parent_id', $ids);

        $ids = $children->pluck('id');
        if (!$ids) {
            return 0;
        }

        return $children->sum('double_vrt') + $this->getIndirectDoubleVrt($ids, $level + 1);
    }
}
