<?php

namespace App\Tasks;

use App\Models\User;
use App\Models\Record;
use App\Support\Currency;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\YecRecord;
use App\Support\Config;

/**
 *YEC限额未入账 定时任务
 *
 * Class
 * @package App\Tasks
 */
class YecQuota
{
    public function __invoke()
    {
        \Log::info('YEC限额未入账 定时任务执行' .date("Y-m-d H:i:s"));
        YecRecord::where(['type'=> YecRecord::TYPE_RECHARGE,'status'=>YecRecord::STATUS_QUOTA_EXAMINE])->each(function ($yecRecord) {
            try {
                DB::transaction(function () use ($yecRecord) {
                    $yecRecord->status =YecRecord::STATUS_CONFIRMED;
                    $yecRecord->save();
                    Record::create([
                            'value' => $yecRecord->enable_value,
                            'type' => '限额充值至 YEC',
                            'user_id' => $yecRecord->user_id,
                            'currency' => Currency::YEC
                        ]);
                });
            } catch (\Throwable $exception) {
                Log::error('yec quota error: ');
            }
        });
        \Log::info('YEC限额未入账 定时任务结束' .date("Y-m-d H:i:s"));
    }
}
