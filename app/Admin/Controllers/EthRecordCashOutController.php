<?php

namespace App\Admin\Controllers;

use App\Models\Notification;
use App\Models\EthRecord;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Admin\Extensions\EthsExporter;

class EthRecordCashOutController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('ETH提现记录')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('ETH提现记录详情')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('ETH提现记录审核')
            ->body($this->form()->edit($id));
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new EthRecord);
        $grid->model()->where(['type' => EthRecord::TYPE_WITHDRAW])->orderBy('status', 'asc');
        $grid->id('ID');
        $grid->column('user.name', '用户名');
        $grid->value('交易金额');
        $grid->column('toAddress.address', '收款地址');
        $grid->from_address('支付地址');
        $grid->enable_value('ETH 实际到账金额');

        $grid->status('审核状态')->display(function ($value) {
            if ($value == EthRecord::STATUS_ETH_CONFIRMED) {
                return '<a class="btn btn-success">' . EthRecord::$statusMap[$value] . '</a>';
            } else {
                if ($value == EthRecord::STATUS_UNCONFIRMED) {
                    return '<a href="/admin/ethrecordcashout/' . $this->id . '/edit" class="btn btn-danger">' . EthRecord::$statusMap[$value] . '</a>';
                } elseif ($value == EthRecord::STATUS_CONFIRMED) {
                    return '<a class="btn btn-primary">' . EthRecord::$statusMap[$value] . '</a>';
                } elseif ($value == EthRecord::STATUS_REJECT) {
                    return '<a class="btn btn-warning">' . EthRecord::$statusMap[$value] . '</a>';
                }
            }
        });
        $grid->created_at('交易时间')->sortable();
        $grid->updated_at('过审时间')->sortable();
        $grid->type('交易类型')->display(function ($value) {
            return EthRecord::$typeMap[$value];
        });
        $grid->filter(function ($filter) {
            // 去掉默认的id过滤器
            $filter->disableIdFilter();
            // 在这里添加字段过滤器
            $filter->like('user.name', '用户名');
            $filter->like('value', '交易金额');
            $filter->equal('status', '审核状态')->radio(EthRecord::$statusMap);
         	$filter->day('created_at', '查询当天');
            $filter->month('updated_at', '查询当月');
        });
        //禁用导出
        // $grid->disableExport();
        //禁用行选择checkbox
        // $grid->disableRowSelector();
        $excel = new EthsExporter();
        $excel->setAttr(
            ['id', '用户','交易金额','实际到账金额','支付地址','收款地址','状态','审核时间'], 
            ['id', 'user.name','value','enable_value','from_address','to_address_id','status','updated_at']);
        $grid->exporter($excel);
        $grid->disableCreateButton();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableView();
            $actions->disableEdit();
            $actions->disableDelete();
        });
       $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });
        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new EthRecord);
        $form->hidden('user_id');
        $form->radio('status', '审核状态')->options(EthRecord::$statusMap);
        $form->decimal('value', '交易金额')->readonly();
        $form->decimal('enable_value', 'ETH 实际到账金额')->readonly();
        $form->display('user_id', '用户名')->with(function () {
            return $this->user->name;
        });
        $form->display('to_address_id', '收款地址')->with(function () {
            return $this->toAddress->address;
        });
        $form->display('type', '交易类型')->with(function ($value) {
            return EthRecord::$typeMap[$value];
        });
        $form->saving(function (Form $form) {
            try {
                $form->model()->markAsRead($form);
            } catch (\Throwable $exception) {
                return redirect('/admin/ethrecordcashout');
            }
        });
        $form->disableReset();
        $form->footer(function ($footer) {
            // 去掉`查看`checkbox
            $footer->disableViewCheck();
            // 去掉`继续编辑`checkbox
            $footer->disableEditingCheck();
            // 去掉`继续创建`checkbox
            $footer->disableCreatingCheck();
        });
        $form->tools(function (Form\Tools $tools) {
            // 去掉`查看`按钮
            $tools->disableView();
        });
        return $form;
    }
}
