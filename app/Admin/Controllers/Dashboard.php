<?php

namespace App\Admin\Controllers;

use Encore\Admin\Admin;

class Dashboard
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function title()
    {
        return view('admin::dashboard.title');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function environment()
    {
        $envs = [
            ['name' => 'Timezone', 'value' => config('app.timezone')],
            ['name' => 'Env',      'value' => config('app.env')],
            ['name' => 'URL',      'value' => config('app.url')],
        ];
        return view('admin::dashboard.environment', compact('envs'));
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function dependencies()
    {
        $json = file_get_contents(base_path('composer.json'));
        $dependencies = json_decode($json, true)['require'];
        return view('admin::dashboard.dependencies', compact('dependencies'));
    }
}
