<?php

namespace App\Admin\Controllers;

use App\Models\ConfigMultipleRule;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;

class ConfigMultipleRuleController extends Controller
{
    /**
     * 转账规则添加修改
     * @param Request $request
     * @return Content
     */
    public function account(Content $content, Request $request)
    {
        $account_id_one = ConfigMultipleRule::where('id', 1)->first(['accounts']);
        if ($request->isMethod('post')) {
            $data = $request->all();
            $account = [
                'to_vrts' => $data['to_vrts'],
                'to_vrt' => $data['to_vrt'],
                'from_vrt' => $data['from_vrt'],
            ];
            $this->common($account, $account_id_one, 'accounts');
        }
        return $content
            ->header('转账规则')
            ->body(view(
                'admin.config.account',
                compact('account_id_one')
            ));
    }

    /**
     * 复投规则添加修改
     * @param Request $request
     * @return Content
     */
    public function throw(Content $content, Request $request)
    {
        $throw_id_one = ConfigMultipleRule::where('id', 1)->first(['throw']);
        if ($request->isMethod('post')) {
            $data = $request->all();
            $throw = [
                'throw_first' => $data['throw_first'],
                'throw_second' => $data['throw_second'],
                'throw_sum' => $data['throw_sum'],
            ];
            $this->common($throw, $throw_id_one, 'throw');
        }
        return $content
            ->header('复投规则')
            ->body(view(
                'admin.config.throw',
                compact('throw_id_one')
            ));
    }

    /**
     * 红包规则添加修改
     * @param Request $request
     * @return Content
     */
    public function staticReleaseTime(Content $content, Request $request)
    {
        $static_release_time = ConfigMultipleRule::where('id', 1)->first(['static_release_time']);
        if ($request->isMethod('post')) {
            $data = $request->all();
            $throw = [
                'effective_time' => $data['effective_time'],
                'release_date' => $data['release_date'],
            ];
            $this->common($throw, $static_release_time, 'static_release_time');
        }
        return $content
            ->header('红包/静态释放规则')
            ->body(view(
                'admin.config.static_release_time',
                compact('static_release_time')
            ));
    }

    /**
     * YECS 静态释放规则 修改添加
     * @param Request $request
     * @return Content
     */
    public function yecsStaticRelease(Content $content, Request $request)
    {
        $yecs_static_release = ConfigMultipleRule::where('id', 1)->first(['yecs_static_release']);
        if ($request->isMethod('post')) {
            $data = $request->all();
            $yecs = [
                'yec_yecs_ratio' => $data['yec_yecs_ratio'], // 释放倍数
                'release_time' => $data['release_time'],   // 释放时间
                'enable' => $data['enable'],         // 是否开启
            ];
            $this->common($yecs, $yecs_static_release, 'yecs_static_release');
        }
        return $content
            ->header('YECS 静态释放规则')
            ->body(view(
                'admin.config.yecs_static_release',
                compact('yecs_static_release')
            ));
    }

    /**
     * YECS 静态释放规则 修改添加
     * @param Request $request
     * @return Content
     */
    public function yecEthRMBRatio(Content $content, Request $request)
    {
        $yec_eth = ConfigMultipleRule::where('id', 1)->first(['yec_eth']);
        if ($request->isMethod('post')) {
            $data = $request->all();
            $yecs = [
                'yec_rmb' => $data['yec_rmb'],
                'eth_rmb' => $data['eth_rmb'],
            ];
            $this->common($yecs, $yec_eth, 'yec_eth');
        }
        return $content
            ->header('YEC/ETH与RMB汇算比率')
            ->body(view(
                'admin.config.yec_eth',
                compact('yec_eth')
            ));
    }

    /**
     * YECS 静态释放规则 修改添加
     * @param Request $request
     * @return Content
     */
    public function Activation(Content $content, Request $request)
    {
        $activation = ConfigMultipleRule::where('id', 1)->first(['activation']);
        if ($request->isMethod('post')) {
            $data = $request->all();
            $yecs = [
                'give' => $data['give'],
                'deduction_yec' => $data['deduction_yec'],
                'deduction_eth' => $data['deduction_eth'],
                'open_activation' => $data['open_activation'],
                'recommendation_rate' => $data['recommendation_rate'],
            ];
            $this->common($yecs, $activation, 'activation');
        }
        return $content
            ->header('激活规则')
            ->body(view(
                'admin.config.activation',
                compact('activation')
            ));
    }
    
    /**
     * 我的各币种兑换比率 修改添加
     * @param Request $request
     * @return Content
     */
    public function MultipleRatios(Content $content, Request $request)
    {
        $multipleratios = ConfigMultipleRule::where('id', 1)->first(['multipleratios']);
        if ($request->isMethod('post')) {
            $data = $request->all();
            $yecs = [
                'yec' => $data['yec'],
                'eth' => $data['eth'],
                'vrts' => $data['vrts'],
                'vrt' => $data['vrt'],
            ];
            $this->common($yecs, $multipleratios, 'multipleratios');
        }
        return $content
            ->header('币种兑换比率')
            ->body(view(
                'admin.config.multipleratios',
                compact('multipleratios')
            ));
    }
    
    /**
     * 动态规则释放方向，VRT或VRTS
     * @param Request $request
     * @return Content
     */
    public function vrtsReprint(Content $content, Request $request)
    {
        $vrtsReprint = ConfigMultipleRule::where('id', 1)->first(['vrtsreprint']);
        if ($request->isMethod('post')) {
            $data = $request->all();
            $yecs = [
                'vrt_vrts' => $data['vrt_vrts'],
            ];
            $this->common($yecs, $vrtsReprint, 'vrtsreprint');
        }
        return $content
            ->header('激活规则')
            ->body(view(
                'admin.config.vrtsreprint',
                compact('vrtsReprint')
            ));
    }

    /**
     * 通用数据添加/更新
     * @param $value
     * @param $res 查询数据是否存在
     */
    protected function common($value, $res, $type)
    {
        if (!$res) {
            $data_res = ConfigMultipleRule::create([$type => $value]);
        } else {
            $data_res = ConfigMultipleRule::find(1)->update([$type => $value]);
        }
        if ($data_res) {
            echo json_encode(array('0' => 'success', '1' => '更新成功'));
            exit();
        }
    }
}
