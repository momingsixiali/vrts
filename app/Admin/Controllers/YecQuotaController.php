<?php

namespace App\Admin\Controllers;

use App\Models\YecRecord;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class YecQuotaController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('YEC限额未入账记录')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('YEC限额未入账记录审核')
            ->body($this->form()->edit($id));
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new YecRecord);
        $grid->model()->where('type', 0)->whereIn('status', [3,4]);
        $grid->id('ID');
        $grid->column('user.name', '用户名');
        $grid->value('交易金额');
        $grid->column('toAddress.address', '收款地址');
        $grid->column('fromAddress.address', '支付地址');
        $grid->enable_value('YEC 实际到账金额');
        $grid->enable_yecs_value('YECS 实际到账金额');

        $grid->type('交易类型')->display(function ($value) {
            return YecRecord::$typeMap[$value];
        });
        $grid->status('审核状态')->display(function ($value) {
            if ($value == YecRecord::STATUS_QUOTA) {
                return '<a href="/admin/yecquota/' . $this->id . '/edit" class="btn btn-danger">' . YecRecord::$statusQuotaMap[$value] . '</a>';
            } elseif ($value == YecRecord::STATUS_QUOTA_EXAMINE) {
                return '<a class="btn btn-primary">' . YecRecord::$statusQuotaMap[$value] . '</a>';
            }
        });
        $grid->created_at('交易时间')->sortable();
        $grid->filter(function ($filter) {
            // 去掉默认的id过滤器
            $filter->disableIdFilter();
            // 在这里添加字段过滤器
            $filter->like('user.name', '用户名');
            $filter->like('value', '交易金额');
        });
        //禁用导出
        $grid->disableExport();
        //禁用行选择checkbox
        $grid->disableRowSelector();
        $grid->disableCreateButton();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableView();
            $actions->disableEdit();
            $actions->disableDelete();
        });
        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new YecRecord);
        $form->radio('status', '审核状态')->options(YecRecord::$statusQuotaMap);
        $form->hidden('user_id');
        $form->decimal('value', '交易金额')->readonly();
        $form->decimal('enable_value', 'YEC 实际到账金额')->readonly();
        $form->decimal('enable_yecs_value', 'YECS 实际到账金额')->readonly();
        $form->display('user_id', '用户名')->with(function () {
            return $this->user->name;
        });
        $form->display('to_address_id', '收款地址')->with(function () {
            return $this->toAddress->address;
        });
        $form->display('form_address_id', '支付地址')->with(function () {
            return $this->fromAddress->address;
        });
        $form->display('type', '交易类型')->with(function ($value) {
            return YecRecord::$typeMap[$value];
        });
        $form->saving(function (Form $form) {
            $form->model()->markAsRead($form);
        });
        $form->disableReset();
        $form->footer(function ($footer) {
            // 去掉`查看`checkbox
            $footer->disableViewCheck();
            // 去掉`继续编辑`checkbox
            $footer->disableEditingCheck();
            // 去掉`继续创建`checkbox
            $footer->disableCreatingCheck();
        });
        $form->tools(function (Form\Tools $tools) {
            // 去掉`查看`按钮
            $tools->disableView();
        });
        return $form;
    }
}
