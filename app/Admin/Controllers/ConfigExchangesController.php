<?php

namespace App\Admin\Controllers;

use App\Models\ConfigExchanges;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ConfigExchangesController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('兑换规则列表')
            ->body($this->grid());
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('兑换规则修改')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('兑换规则创建')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ConfigExchanges);
        $grid->model()->orderBy('enabled', 'desc');
        $grid->id('Id')->sortable();
        $grid->type('兑换类型')->display(function ($value) {
            return ConfigExchanges::$typeMap[$value];
        });
        $grid->tip('兑换提示语');
        $grid->exchange_value('兑换比率')->display(function ($value) {
            return str_replace('value', str_replace('.00000000', '', $value), ConfigExchanges::$typeValue[$this->type]);
        });
        $grid->not_before('生效时间')->sortable();
        $grid->created_at('设定时间');
        $grid->enabled('是否启用')->display(function ($value) {
            return $value ? '是' : '否';
        });
        //禁用查询过滤器
        $grid->disableFilter();
        //禁用导出
        $grid->disableExport();
        //禁用行选择checkbox
        $grid->disableRowSelector();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableView();
        });
        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ConfigExchanges);
        $form->select('type', '类型')->options(ConfigExchanges::$typeMap)->rules('required')->default(ConfigExchanges::TYPE_YEC_VRT);
        $form->text('tip', '兑换提示语')->rules('required|min:7|max:25', [
            'min' => '请输入如(兑换5倍VRT)的提示语',
        ]);
        $form->decimal('eth_rmb', '兑换币种与RMB比率')->rules('required|numeric|min:0');
        $form->decimal('yec_rmb', '被兑换币种与RMB比率')->rules('required|numeric|min:0');
        $form->decimal('exchange_value', '兑换值')->rules('required|numeric|min:0')->readOnly();
        $form->datetime('not_before', '生效时间')->rules('required');
        $form->switch('enabled', '是否启用');
        Admin::script('
                        $("#yec_rmb").blur(function(){
                            var eth_rmb = $("#eth_rmb").val();
                            var yec_rmb = $("#yec_rmb").val();
                            var value = (eval(eth_rmb*100000) / eval(yec_rmb*100000));
                            $("#exchange_value").val(value);
                        });
                
                        $("#eth_rmb").blur(function(){
                            var eth_rmb = $("#eth_rmb").val();
                            var yec_rmb = $("#yec_rmb").val();
                            var value = (eval(eth_rmb*100000) / eval(yec_rmb*100000));
                            $("#exchange_value").val(value);
                        }); 
        ');
        $form->footer(function ($footer) {
            // 去掉`查看`checkbox
            $footer->disableViewCheck();
            // 去掉`继续编辑`checkbox
            $footer->disableEditingCheck();
            // 去掉`继续创建`checkbox
//            $footer->disableCreatingCheck();
        });
        $form->tools(function (Form\Tools $tools) {
            // 去掉`查看`按钮
            $tools->disableView();
        });
        return $form;
    }
}
