<?php

namespace App\Admin\Controllers;

use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use App\Models\Notification;

class NotificationsController extends Controller
{

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content, Notification $notification)
    {
        // 获取未读所有通知
        $notifications = $notification->whereNull('read_at')->paginate();
        return $content
            ->header('通知页面')
            ->body(view('admin.notifications.index', compact('notifications')));
    }
}
