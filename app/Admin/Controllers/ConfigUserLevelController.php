<?php

namespace App\Admin\Controllers;

use App\Models\ConfigDynamicRelease;
use App\Models\ConfigStaticRelease;
use App\Models\ConfigUserLevel;
use App\Http\Controllers\Controller;
use App\Models\ConfigYecsStaticRatio;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ConfigUserLevelController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('用户等级设定')
            ->body($this->grid());
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('用户等级设定创建')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ConfigUserLevel);
        $grid->model()->orderBy('scale', 'asc');
        $grid->id('ID')->sortable();
        $grid->level('等级');
        $grid->scale('比例')->sortable();
        $grid->vrt_set('VRT 金额设定');
        $grid->vrt_direct_total('直推VRT总和');
        $grid->vrt_indirect_total('间推VRT总和');
        $grid->vrt_direct_repeat_total('直推复投VRT总和');
        $grid->vrt_indirect_repeat_total('间推复投VRT总和');
        $grid->computational_algebra('计算代数_推荐关系');
        $grid->created_at('创建时间');

        //禁用查询过滤器
        $grid->disableFilter();
        //禁用导出
        $grid->disableExport();
        //禁用行选择checkbox
        $grid->disableRowSelector();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableView();
            $actions->disableDelete();
        });

        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ConfigUserLevel);
        $form->text('level', '等级')->rules(function ($form) {
            if ($id = $form->model()->id) {
                return 'required|unique:config_user_levels,level,' . $id . ',id';
            } else {
                return 'required|unique:config_user_levels';
            }
        });
        if (request()->route()->parameters()) {
            $form->text('scale', '比例')->readOnly();
        } else {
            $form->text('scale', '比例')->rules(function ($form) {
                if ($id = $form->model()->id) {
                    return 'required|numeric|min:0|max:100|unique:config_user_levels,scale,' . $id . ',id';
                } else {
                    return 'required|numeric|unique:config_user_levels|min:0|max:100';
                }
            });
        }
        $form->text('vrt_set', 'VRT 金额设定')->rules('required|numeric|min:0');
        $form->text('vrt_direct_total', '直推VRT总和')->rules('required|numeric|min:0');
        $form->text('vrt_indirect_total', '间推VRT总和')->rules('required|numeric|min:0');
        $form->text('vrt_direct_repeat_total', '直推复投VRT总和')->rules('required|numeric|min:0');
        $form->text('vrt_indirect_repeat_total', '间推复投VRT总和')->rules('required|numeric|min:0');
        $form->number('computational_algebra', '计算代数')->rules('required')->min(1)->max(100);
        $form->hidden('ConfigDynamicRelease.level');
        $form->hidden('ConfigStaticRelease.level');
        $form->hidden('ConfigYecsStaticRatio.level');
        Admin::script('
            $("form").submit(function(){
                var level = $("#level").val();
                $(".ConfigDynamicRelease_level_").val(level);
                $(".ConfigStaticRelease_level_").val(level);
                $(".ConfigYecsStaticRatio_level_").val(level);
            });
        ');

        $form->footer(function ($footer) {
            // 去掉`查看`checkbox
            $footer->disableViewCheck();
            // 去掉`继续编辑`checkbox
            $footer->disableEditingCheck();
            // 去掉`继续创建`checkbox
            $footer->disableCreatingCheck();
        });
        $form->tools(function (Form\Tools $tools) {
            // 去掉`查看`按钮
            $tools->disableView();
            $tools->disableDelete();
        });
        return $form;
    }
}
