<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Admin\Controllers\Dashboard;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;

class HomeController extends Controller
{
    public function index(Content $content)
    {
        $content->header('首页');
        // 添加面包屑导航 since v1.5.7
        $content->breadcrumb(
            ['text' => '首页', 'url' => '/admin']
        );
        // $content->row(function (Row $row) {
        //     $row->column(4, 'xxx');
        //     $row->column(8, function (Column $column) {
        //         $column->row('111');
        //         $column->row('222');
        //         $column->row('333');
        //     });
        // });
        return $content;
    }
}
