<?php

namespace App\Admin\Controllers;

use App\Models\Notification;
use App\Models\YecRecord;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class YecRecordController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('YEC充值记录')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('YEC充值记录详情')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('YEC充值记录审核')
            ->body($this->form()->edit($id));
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new YecRecord);
        $grid->model()->where([['type', 0],['status','<',3]])->orderBy('status', 'asc');
        $grid->id('ID');
        $grid->column('user.name', '用户名');
        $grid->value('交易金额');
        $grid->column('toAddress.address', '收款地址');
        $grid->column('fromAddress.address', '支付地址');
        $grid->enable_value('YEC 实际到账金额');
        $grid->enable_yecs_value('YECS 实际到账金额');

        $grid->type('交易类型')->display(function ($value) {
            return YecRecord::$typeMap[$value];
        });
        $grid->status('审核状态')->display(function ($value) {
            if ($value == YecRecord::STATUS_UNCONFIRMED) {
                return '<a href="/admin/yecrecord/' . $this->id . '/edit" class="btn btn-danger">' . YecRecord::$statusMap[$value] . '</a>';
            } elseif ($value == YecRecord::STATUS_CONFIRMED) {
                return '<a class="btn btn-primary">' . YecRecord::$statusMap[$value] . '</a>';
            } elseif ($value == YecRecord::STATUS_REJECT) {
                return '<a class="btn btn-warning">' . YecRecord::$statusMap[$value] . '</a>';
            }
        });
        $grid->created_at('交易时间')->sortable();
        $grid->updated_at('过审时间')->sortable();
        $grid->filter(function ($filter) {
            // 去掉默认的id过滤器
            $filter->disableIdFilter();
            // 在这里添加字段过滤器
            $filter->like('user.name', '用户名');
            $filter->like('value', '交易金额');
            $filter->equal('status', '记录状态')->radio(YecRecord::$statusMap);
        });
        //禁用导出
        $grid->disableExport();
        //禁用行选择checkbox
        $grid->disableRowSelector();
        $grid->disableCreateButton();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableView();
            $actions->disableEdit();
            $actions->disableDelete();
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(YecRecord::findOrFail($id));

        $show->user_id('用户名')->as(function () {
            return $this->user->name;
        });
        $show->value('交易金额');
        $show->to_address_id('收款地址');
        $show->from_address_id('支付地址');
        $show->type('交易类型')->as(function ($type) {
            return YecRecord::$typeMap[$type];
        });

        $show->status('是否审核')->as(function ($value) {
            return $value ? '已审核' : '待审核';
        });
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new YecRecord);
        $form->radio('status', '审核状态')->options(YecRecord::$statusMap);
        $form->hidden('user_id');
        $form->decimal('value', '交易金额')->readonly();
        $form->decimal('enable_value', 'YEC 实际到账金额')->readonly();
        $form->decimal('enable_yecs_value', 'YECS 实际到账金额')->readonly();
        $form->display('user_id', '用户名')->with(function () {
            return $this->user->name;
        });
        $form->display('to_address_id', '收款地址')->with(function () {
            return $this->toAddress->address;
        });
        $form->display('form_address_id', '支付地址')->with(function () {
            return $this->fromAddress->address;
        });
        $form->display('type', '交易类型')->with(function ($value) {
            return YecRecord::$typeMap[$value];
        });
        $form->saving(function (Form $form) {
            $form->model()->markAsRead($form);
        });
        $form->disableReset();
        $form->footer(function ($footer) {
            // 去掉`查看`checkbox
            $footer->disableViewCheck();
            // 去掉`继续编辑`checkbox
            $footer->disableEditingCheck();
            // 去掉`继续创建`checkbox
            $footer->disableCreatingCheck();
        });
        $form->tools(function (Form\Tools $tools) {
            // 去掉`查看`按钮
            $tools->disableView();
        });
        return $form;
    }
}
