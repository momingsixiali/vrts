<?php

namespace App\Admin\Controllers;

use App\Models\ConfigRecharge;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ConfigRechargeController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('充币规则')
            ->body($this->grid());
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('充币规则修改')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('充币规则创建')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ConfigRecharge);
        $grid->model()->orderBy('created_at', 'desc');
        $grid->id('Id')->sortable();
        $grid->type('充值类型')->display(function ($value) {
            return ConfigRecharge::$typeMap[$value];
        });
        $grid->column('quota_surplus_ratio', '今日限额/今日余额/已使用率')->display(function () {
//            return "{$this->quota} / {$this->quota_surplus} / ".round((1-$this->quota_surplus/$this->quota)*100,2).'%';
            return str_replace('.00000000', '', $this->quota) . '/' . str_replace('.00000000', '', $this->quota_surplus) . '/' . round((1 - $this->quota_surplus / $this->quota) * 100, 2) . '%';

        });
        $grid->opening_time('生效时间');
        $grid->enabled('是否启用')->display(function ($value) {
            return $value ? '是' : '否';
        });
        $grid->created_at('设定时间');

        //禁用查询过滤器
        $grid->disableFilter();
        //禁用导出
        $grid->disableExport();
        //禁用行选择checkbox
        $grid->disableRowSelector();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableView();
        });
        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ConfigRecharge);
        $form->select('type', '类型')->options(ConfigRecharge::$typeMap)->rules('required')->default(ConfigRecharge::TYPE_YEC);
        $form->decimal('quota', '每日限定充值金额')->rules('required|numeric|min:0|max:1000000000', [
            'max' => '每日充值金额不能大于10亿',
        ]);
        $form->hidden('quota_surplus', '余额');
        $form->datetime('opening_time', '开启时间')->rules('required');
        $form->switch('enabled', '是否开启');
        $form->saving(function (Form $form) {
            $form->quota_surplus = $form->quota;
        });
        $form->footer(function ($footer) {
            // 去掉`查看`checkbox
            $footer->disableViewCheck();
            // 去掉`继续编辑`checkbox
            $footer->disableEditingCheck();
            // 去掉`继续创建`checkbox
//            $footer->disableCreatingCheck();
        });
        $form->tools(function (Form\Tools $tools) {
            // 去掉`查看`按钮
            $tools->disableView();
        });
        return $form;
    }
}
