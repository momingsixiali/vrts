<?php

namespace App\Admin\Controllers;

use App\Models\Record;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Support\Currency;

class YecsRecordController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('YECS资金流动')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('YECS资金流动详情')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('YECS资金流动修改')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('YECS资金流动创建')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Record());
        $grid->model()->where(['currency' => Currency::YECS]);
        $grid->type('交易类型');
        $grid->id('ID');
        $grid->user_id('账号信息');
        $grid->column('user.name', '账号用户名');
        $grid->value('入/出账金额');
        $grid->column('address', '转出账号信息')->display(function () {
            return "系统";
        });
        $grid->column('ip', 'IP')->display(function () {
            return "127.0.0.1";
        });
        $grid->column('GPS', 'GPS')->display(function () {
            return "66.0001:45.001";
        });
        $grid->created_at('交易时间')->sortable();
        $grid->filter(function ($filter) {
            // 去掉默认的id过滤器
            $filter->disableIdFilter();
            // 在这里添加字段过滤器
            $filter->like('user.name', '用户名');
            $filter->like('value', '交易金额');
        });
        //禁用导出
        $grid->disableExport();
        //禁用行选择checkbox
        $grid->disableRowSelector();
        $grid->disableCreateButton();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableView();
            $actions->disableEdit();
            $actions->disableDelete();
        });
        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Record);

        $form->number('user_id', 'User id');
        $form->decimal('value', 'Value');
        $form->number('from_address_id', 'From address id');
        $form->number('to_address_id', 'To address id');
        $form->decimal('enable_value', 'Enable value');
        $form->decimal('enable_yecs_value', 'Enable yecs value')->default(0.00000000);
        $form->switch('status', 'Status');
        $form->switch('type', 'Type');

        return $form;
    }
}
