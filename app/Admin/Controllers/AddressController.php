<?php

namespace App\Admin\Controllers;

use App\Models\Address;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\MessageBag;
use SimpleSoftwareIO\QrCode\Facades\QrCode;


class AddressController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('平台地址')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('平台地址修改')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('平台地址创建')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Address);
        $grid->model()->where(['type'=>0,'currency'=>1,'user_id'=>0]);
        $grid->id('Id');
        $grid->address('YEC收款地址');
        $grid->type('平台地址')->display(function ($value){
            return Address::$currencyMap[$this->currency]. Address::$typeMap[$value];
        });
        $grid->column('qrcode')->display(function ($value) {
            return "<img style='width: 100px;height: 100px;' src='{$value}'/>";
        });
        $grid->status('是否启用')->display(function ($value){
            return $value?'已启用':'未启用';
        });
        $grid->created_at('创建时间');
        //禁用查询过滤器
        $grid->disableFilter();
        //禁用导出
        $grid->disableExport();
        //禁用行选择checkbox
        $grid->disableRowSelector();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableView();
        });
        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Address);
        $form->text('address','YEC收款地址')->rules(function ($form) {
            if ($id = $form->model()->id) {
                return 'required|unique:addresses,address,'.$id.',id';
            } else {
                return 'required|unique:addresses';
            }
        });
        $form->image('qrcode','YEC二维码地址')->uniqueName()->rules('required|image|dimensions:min_width=224,min_height=224',[
            'dimensions' => '图片的清晰度不够，宽和高需要 224px 以上',
        ])->move('/qrcodes');
        $form->switch('status','是否启用');
        $form->hidden('type')->value(0);
        $form->hidden('user_id')->value(0);
        $form->hidden('currency')->value(1);

        $form->saving(function (Form $form) {
            #验证值是否有重复
            $meg = [
                'title'=>'提示',
                'message'=>'只允许启用一个账号!如需更换账号请先关闭已启用的账号',
            ];
            $data = $form->model()->select('id','status')->where(['status'=>1,'type'=>Address::TYPE_PLATFORM,'currency'=>Address::CURRENCY_YEC])->first();
            if($data && $form->model()->id){
                if ($data->id==$form->model()->id) {
                }else{
                    if ($form->status=='on') {
                        $error = new MessageBag($meg);
                        return back()->withInput()->with(compact('error'));
                    }
                }
            }
            if ($data && !$form->model()->id) {
                $error = new MessageBag($meg);
                return back()->withInput()->with(compact('error'));
            }
        });
        $form->footer(function ($footer) {
            // 去掉`查看`checkbox
            $footer->disableViewCheck();
            // 去掉`继续编辑`checkbox
            $footer->disableEditingCheck();
            // 去掉`继续创建`checkbox
            $footer->disableCreatingCheck();
        });
        $form->tools(function (Form\Tools $tools) {
            // 去掉`查看`按钮
            $tools->disableView();
        });
        return $form;
    }
}
