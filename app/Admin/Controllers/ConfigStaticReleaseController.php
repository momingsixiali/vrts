<?php

namespace App\Admin\Controllers;

use App\Models\ConfigStaticRelease;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ConfigStaticReleaseController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('静态释放管理')
            ->body($this->grid());
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('静态释放规则修改')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('静态释放规则创建')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ConfigStaticRelease);
        $grid->id('ID')->sortable();
        $grid->level('等级');
        $grid->vrt_one('VRT<20%释放比例')->display(function ($value) {
            return str_replace('.00', '', $value . '%');
        });
        $grid->vrt_two('VRT<40%释放比例')->display(function ($value) {
            return str_replace('.00', '', $value . '%');
        });
        $grid->vrt_five('VRT<60%释放比例')->display(function ($value) {
            return str_replace('.00', '', $value . '%');
        });
        $grid->vrt_three('VRT<80%释放比例')->display(function ($value) {
            return str_replace('.00', '', $value . '%');
        });
        $grid->vrt_four('VRT<100%释放比例')->display(function ($value) {
            return str_replace('.00', '', $value . '%');
        });
        $grid->created_at('创建时间');
        //禁用查询过滤器
        $grid->disableFilter();
        //禁用导出
        $grid->disableExport();
        $grid->disableCreateButton();

        //禁用行选择checkbox
        $grid->disableRowSelector();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableView();
            $actions->disableDelete();
        });
        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ConfigStaticRelease);
        $form->text('level', '等级')->rules(function ($form) {
            if ($id = $form->model()->id) {
                return 'required|unique:config_static_releases,level,' . $id . ',id';
            } else {
                return 'required|unique:config_static_releases';
            }
        })->readOnly();
        $form->decimal('vrt_one', 'VRT<20%释放比例')->default(0.00)->rules('required|numeric|min:0.00|max:100');
        $form->decimal('vrt_two', 'VRT<40%释放比例')->default(0.00)->rules('required|numeric|min:0.00|max:100');
        $form->decimal('vrt_three', 'VRT<60%释放比例')->default(0.00)->rules('required|numeric|min:0.00|max:100');
        $form->decimal('vrt_four', 'VRT<80%释放比例')->default(0.00)->rules('required|numeric|min:0.00|max:100');
        $form->decimal('vrt_five', 'VRT<100%释放比例')->default(0.00)->rules('required|numeric|min:0.00|max:100');
        $form->footer(function ($footer) {
            // 去掉`查看`checkbox
            $footer->disableViewCheck();
            // 去掉`继续编辑`checkbox
            $footer->disableEditingCheck();
            // 去掉`继续创建`checkbox
//            $footer->disableCreatingCheck();
        });
        $form->tools(function (Form\Tools $tools) {
            // 去掉`查看`按钮
            $tools->disableView();
            $tools->disableDelete();
        });
        return $form;
    }
}
