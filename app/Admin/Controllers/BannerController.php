<?php

namespace App\Admin\Controllers;

use App\Models\Banner;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class BannerController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Banner列表')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Banner详情')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Banner修改')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Banner创建')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Banner);

        $grid->id('ID');
        $grid->column('path')->display(function ($value) {
            return "<img style='width: 500px;height: 130px;' src='{$value}'/>";
        });
        $grid->type('图片类型')->display(function ($value) {
            return Banner::$typeMap[$value];
        });
        $grid->created_at('创建时间');
        //禁用查询过滤器
        $grid->disableFilter();
        //禁用导出
        $grid->disableExport();
        //禁用行选择checkbox
        $grid->disableRowSelector();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
//            $actions->disableView();
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Banner::findOrFail($id));
        $show->type('图片类型')->as(function ($value) {
            return Banner::$typeMap[$value];
        });
        $show->path('图片')->setEscape(false)->as(function ($value) {
            return "<img src='{$value}'/>";
        });
        $show->enabled('是否启用')->as(function ($value) {
            return $value ? '启用' : '未启用';
        });
        $show->created_at('创建时间');
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Banner);
        $form->image('path', 'Banner图片')->uniqueName()->rules('required|image|dimensions:min_width=208,min_height=192', [
            'dimensions' => '图片的清晰度不够，宽和高需要 192px 以上',
        ])->move('/banner');
        ;
        $form->select('type', '图片类型')->options(Banner::$typeMap)->default(1);
        $form->switch('enabled', '是否启用');
        $form->footer(function ($footer) {
            // 去掉`查看`checkbox
            $footer->disableViewCheck();
            // 去掉`继续编辑`checkbox
            $footer->disableEditingCheck();
            // 去掉`继续创建`checkbox
            $footer->disableCreatingCheck();
        });
        $form->tools(function (Form\Tools $tools) {
            // 去掉`查看`按钮
            $tools->disableView();
        });
        return $form;
    }
}
