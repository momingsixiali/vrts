<?php

namespace App\Admin\Controllers;

use App\Models\ConfigYecsStaticRatio;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ConfigYecsStaticRatioController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('YECS静态释放规则')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('YECS静态释放规则修改')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('YECS静态释放规则创建')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ConfigYecsStaticRatio);
        $grid->id('ID')->sortable();
        $grid->level('等级');
        $grid->yecs_one('YECS < 20%释放比例')->display(function ($value) {
            return str_replace('.00', '', $value . '%');
        });
        $grid->yecs_two('YECS < 40%释放比例')->display(function ($value) {
            return str_replace('.00', '', $value . '%');
        });
        $grid->yecs_five('YECS < 60%释放比例')->display(function ($value) {
            return str_replace('.00', '', $value . '%');
        });
        $grid->yecs_three('YECS < 80%释放比例')->display(function ($value) {
            return str_replace('.00', '', $value . '%');
        });
        $grid->yecs_four('YECS < 100%释放比例')->display(function ($value) {
            return str_replace('.00', '', $value . '%');
        });
        $grid->created_at('创建时间');
        //禁用查询过滤器
        $grid->disableFilter();
        //禁用导出
        $grid->disableExport();
        $grid->disableCreateButton();

        //禁用行选择checkbox
        $grid->disableRowSelector();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableView();
            $actions->disableDelete();
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ConfigYecsStaticRatio::findOrFail($id));
        $show->id('Id');
        $show->level('Level');
        $show->yecs_one('Yecs one');
        $show->yecs_two('Yecs two');
        $show->yecs_three('Yecs three');
        $show->yecs_four('Yecs four');
        $show->yecs_five('Yecs five');
        $show->created_at('Created at');
        $show->updated_at('Updated at');
        $show->deleted_at('Deleted at');
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ConfigYecsStaticRatio);
        $form->text('level', '等级')->rules(function ($form) {
            if ($id = $form->model()->id) {
                return 'required|unique:config_yecs_static_ratios,level,' . $id . ',id';
            } else {
                return 'required|unique:config_yecs_static_ratios';
            }
        })->readOnly();
        $form->decimal('yecs_one', 'YECS<20%释放比例')->default(0.00)->rules('required|numeric|min:0.00|max:100');
        $form->decimal('yecs_two', 'YECS<40%释放比例')->default(0.00)->rules('required|numeric|min:0.00|max:100');
        $form->decimal('yecs_three', 'YECS<60%释放比例')->default(0.00)->rules('required|numeric|min:0.00|max:100');
        $form->decimal('yecs_four', 'YECS<80%释放比例')->default(0.00)->rules('required|numeric|min:0.00|max:100');
        $form->decimal('yecs_five', 'YECS<100%释放比例')->default(0.00)->rules('required|numeric|min:0.00|max:100');
        $form->footer(function ($footer) {
            // 去掉`查看`checkbox
            $footer->disableViewCheck();
            // 去掉`继续编辑`checkbox
            $footer->disableEditingCheck();
            // 去掉`继续创建`checkbox
            $footer->disableCreatingCheck();
        });
        $form->tools(function (Form\Tools $tools) {
            // 去掉`查看`按钮
            $tools->disableView();
            $tools->disableDelete();
        });
        return $form;
    }
}
