<?php

namespace App\Admin\Controllers;

use App\Models\Notification;
use App\Models\EthRecord;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class EthRecordController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('ETH充值记录')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('ETH充值记录详情')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('ETH充值记录审核')
            ->body($this->form()->edit($id));
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new EthRecord);
        $grid->model()->where(['type' => 0])->orderBy('status', 'asc');
        $grid->id('ID');
        $grid->column('user.name', '用户名');
        $grid->value('交易金额');
        $grid->column('toAddress.address', '收款地址');
        $grid->from_address('支付地址');
        $grid->enable_value('ETH 实际到账金额');

        $grid->status('审核状态')->display(function ($value) {
            if ($value == EthRecord::STATUS_ETH_CONFIRMED) {
                return '<a class="btn btn-success">' . EthRecord::$statusMap[$value] . '</a>';
            } else {
                if ($value == EthRecord::STATUS_UNCONFIRMED) {
                    return '<a href="/admin/ethrecord/' . $this->id . '/edit" class="btn btn-danger">' . EthRecord::$statusMap[$value] . '</a>';
                } elseif ($value == EthRecord::STATUS_CONFIRMED) {
                    return '<a class="btn btn-primary">' . EthRecord::$statusMap[$value] . '</a>';
                } elseif ($value == EthRecord::STATUS_REJECT) {
                    return '<a class="btn btn-warning">' . EthRecord::$statusMap[$value] . '</a>';
                }
            }
        });
        $grid->created_at('交易时间')->sortable();
        $grid->updated_at('过审时间')->sortable();
        $grid->type('交易类型')->display(function ($value) {
            return EthRecord::$typeMap[$value];
        });
        $grid->filter(function ($filter) {
            // 去掉默认的id过滤器
            $filter->disableIdFilter();
            // 在这里添加字段过滤器
            $filter->like('user.name', '用户名');
            $filter->like('value', '交易金额');
            $filter->equal('status', '审核状态')->radio(EthRecord::$statusMap);
        });
        //禁用导出
        $grid->disableExport();
        //禁用行选择checkbox
        $grid->disableRowSelector();
        $grid->disableCreateButton();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableView();
            $actions->disableEdit();
            $actions->disableDelete();
        });
        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
//        dd(EthRecord::first()->toAddress->address);
        $form = new Form(new EthRecord);
        $form->hidden('user_id');
        $form->radio('status', '审核状态')->options(EthRecord::$statusMap);
        $form->decimal('value', '交易金额')->readonly();
        $form->decimal('enable_value', 'ETH 实际到账金额')->readonly();
        $form->display('user_id', '用户名')->with(function () {
            return $this->user->name;
        });
        $form->display('to_address_id', '收款地址')->with(function () {
            return $this->toAddress->address;
        });
        $form->display('type', '交易类型')->with(function ($value) {
            return EthRecord::$typeMap[$value];
        });
        $form->saving(function (Form $form) {
            $form->model()->markAsRead($form);
        });
        $form->disableReset();
        $form->footer(function ($footer) {
            // 去掉`查看`checkbox
            $footer->disableViewCheck();
            // 去掉`继续编辑`checkbox
            $footer->disableEditingCheck();
            // 去掉`继续创建`checkbox
            $footer->disableCreatingCheck();
        });
        $form->tools(function (Form\Tools $tools) {
            // 去掉`查看`按钮
            $tools->disableView();
        });
        return $form;
    }
}
