<?php

namespace App\Admin\Controllers;

use App\Models\Packet;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Support\Currency;

class YecsStaticController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('YECS静态释放')
            ->body($this->grid());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Packet);
        $grid->model()->where(['currency' => Currency::YECS])->orderBy('created_at', 'desc');

        $grid->id('ID');
        $grid->column('user.name', '用户账号');
        $grid->value('签到金额');
        $grid->after('签到后YEC');
        $grid->before('签到前YEC');
        $grid->after_s('释放后金额');
        $grid->before_s('释放前金额');
        $grid->status('红包状态')->display(function ($value) {
            return Packet::$statusMap[$value];
        });
        $grid->created_at('释放时间');
        $grid->expired_at('过期时间时间');
        $grid->updated_at('领取时间');
        
        $grid->filter(function ($filter) {
            // 去掉默认的id过滤器
            $filter->disableIdFilter();
            // 在这里添加字段过滤器
            $filter->like('user.name', '用户名');
            $filter->like('value', '交易金额');
            $filter->equal('status', '红包状态')->radio(Packet::$statusMap);
        });
        //禁用导出
        $grid->disableExport();
        //禁用行选择checkbox
        $grid->disableRowSelector();
        $grid->disableCreateButton();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableView();
            $actions->disableEdit();
            $actions->disableDelete();
        });
        return $grid;
    }
}
