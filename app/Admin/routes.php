<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();
//Route::post('/account',config('admin.route.namespace').'\ConfigMultipleRuleController@account')->name('admin.account');

Route::group([
    'prefix' => config('admin.route.prefix'),
    'namespace' => config('admin.route.namespace'),
    'middleware' => config('admin.route.middleware'),
], function (Router $router) {
    $router->get('/', 'HomeController@index');
    $router->resources([
        'users' => UsersController::class,
        'userlevelset' => ConfigUserLevelController::class,
        'dynamicrelease' => ConfigDynamicReleaseController::class,
        'staticrelease' => ConfigStaticReleaseController::class,
        'yecsstaticratio' => ConfigYecsStaticRatioController::class,
        'exchange' => ConfigExchangesController::class,
        'recharge' => ConfigRechargeController::class,
        'ethrecord' => EthRecordController::class,
        'yecrecord' => YecRecordController::class,
        'yecsrecord' => YecsRecordController::class,
        'yecrecordcashout' => YecRecordCashOutController::class,
        'yecsstatic'=>  YecsStaticController::class,
        'yecquota'=> YecQuotaController::class,
        'ethrecord' => EthRecordController::class,
        'ethrecordcashout' => EthRecordCashOutController::class,
        'banner' => BannerController::class,
        'address' => AddressController::class,
        'version' => VersionController::class,
    ]);
    $router->get('/relation/{id}', 'UsersController@getUserRelations')->name('admin.relation');
    $router->get('/account', 'ConfigMultipleRuleController@account')->name('admin.account');
    $router->post('/account', 'ConfigMultipleRuleController@account')->name('admin.account');
    $router->get('/throw', 'ConfigMultipleRuleController@throw')->name('admin.throw');
    $router->post('/throw', 'ConfigMultipleRuleController@throw')->name('admin.throw');
    $router->get('/staticreleasetime', 'ConfigMultipleRuleController@staticreleasetime')->name('admin.staticreleasetime');
    $router->post('/staticreleasetime', 'ConfigMultipleRuleController@staticreleasetime')->name('admin.staticreleasetime');
    $router->get('/yecsstaticrelease', 'ConfigMultipleRuleController@yecsstaticrelease')->name('admin.yecsstaticrelease');
    $router->post('/yecsstaticrelease', 'ConfigMultipleRuleController@yecsstaticrelease')->name('admin.yecsstaticrelease');
    $router->resource('/notifications', 'NotificationsController', ['only' => ['index']]);

    $router->get('/vrtsreprint', 'ConfigMultipleRuleController@vrtsReprint');
    $router->post('/vrtsreprint', 'ConfigMultipleRuleController@vrtsReprint');
    // $router->resource('/notifications', 'NotificationsController', ['only' => ['index']]);
    $router->get('/yeceth', 'ConfigMultipleRuleController@yecEthRMBRatio')->name('admin.yeceth');
    $router->post('/yeceth', 'ConfigMultipleRuleController@yecEthRMBRatio')->name('admin.yeceth');
    $router->get('/activation', 'ConfigMultipleRuleController@activation')->name('admin.activation');
    $router->post('/activation', 'ConfigMultipleRuleController@activation')->name('admin.activation');
    
    $router->get('/multipleratios', 'ConfigMultipleRuleController@multipleratios')->name('admin.multipleratios');
    $router->post('/multipleratios', 'ConfigMultipleRuleController@multipleratios')->name('admin.multipleratios');
    $router->get('/users/vrtsstatus/{id}', 'UsersController@vrtsAccountsStatus')->name('admin.vrtsstatus');
    $router->get('/users/staticstatus/{id}', 'UsersController@staticStatus')->name('admin.staticstatus');
    $router->get('/users/dynamicstatus/{id}', 'UsersController@dynamicStatus')->name('admin.dynamicstatus');
});
