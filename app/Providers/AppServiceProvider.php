<?php

namespace App\Providers;

use App\Models\User;
use App\Models\Record;
use App\Models\YecRecord;
use App\Models\VrtsRecord;
use App\Observers\UserObserver;
use App\Observers\RecordObserver;
use App\Observers\YecRecordObserver;
use App\Observers\VrtsRecordObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Resources\Json\Resource;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('letter', function ($attribute, $value, $parameters, $validator) {
            /**
             * @var $validator \Illuminate\Validation\Validator
             */
            $validator->setCustomMessages(['letter' => '用户名必须是英文字符和数字！']);
            return is_string($value) && preg_match('/^[0-9a-zA-Z_-]+$/u', $value);
        });
        VrtsRecord::observe(VrtsRecordObserver::class);
        YecRecord::observe(YecRecordObserver::class);
        Record::observe(RecordObserver::class);
        User::observe(UserObserver::class);
        Resource::withoutWrapping();
    }
}
