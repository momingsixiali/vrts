<?php

namespace App\Providers;

use JPush\Client;
use Illuminate\Support\ServiceProvider;

class JpushServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('jpush', function ($app) {
            return new Client(
                $app['config']['services.jpush.app_key'],
                $app['config']['services.jpush.master_secret'],
                storage_path('logs/jpush.log')
            );
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
