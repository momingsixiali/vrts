<?php
return [
    'to_from_type' => [
        // 转入币种
        'to_type' => [
            ['to_key'  => 1,'to_type'=>'YEC'],
            ['to_key'  => 3,'to_type'=>'VRTS'],
            ['to_key'  => 4,'to_type'=>'ETH'],
        ],
        // 转出币种
        'from_type' => [
            ['from_key'  => 1,'from_type'=>'YEC'],
            ['from_key'  => 2,'from_type'=>'VRT'],
            ['from_key'  => 3,'from_type'=>'VRTS'],
            ['from_key'  => 4,'from_type'=>'ETH'],
        ],
    ],
    'ConfigMultipleRule'=>1,
    // 是不是复投
    'is_indirect' => false
];
